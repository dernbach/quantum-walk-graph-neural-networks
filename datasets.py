import os

import networkx as nx
import numpy as np
import scipy as sp
import scipy.io
import torch.nn as nn
from sklearn.cluster import KMeans
from torch.utils.data import Dataset

import nnUtils
from constructions import adj2amps, adj2swap
from utils import label2vec

"""
Specific Datasets should be implemented as functions that return an X, Y, and adj as lists of arrays
y should be returned as a class index, not a one-hot-vector for classification problems
"""

class graphDataset(Dataset):
    def __init__(self,X,Y,adj,amps=None,swap=None,transform=None,**kwargs):
        super(graphDataset, self).__init__()
        self.X=X
        self.Y=Y
        self.adj=adj
        self.nodes=adj.shape[0]
        self.degrees=np.sum(self.adj,1)
        self.maxDegree=np.max(self.degrees)
        if amps:
            self.amps=amps
        else:
            self.amps=adj2amps(self.adj,self.maxDegree)
        if swap:
            self.swap=swap
        else:
            self.swap=adj2swap(self.adj,self.maxDegree)
        self.transform=transform

    def shuffleExamples_(self):
        perm=np.random.permutation(len(self))
        self.X=[self.X[i] for i in perm]
        self.Y=[self.Y[i] for i in perm]

    def shuffleNodes(self):
        #perm=np.random.permutation(len(self.adj))
        #self._orderNodes(perm)
        return

    def reorderNodes(self,orderFunc):
        g = nx.from_numpy_matrix(self.adj)
        vals=orderFunc(g)
        vals=[vals[i] for i in range(self.nodes)]
        ord=np.argsort(vals)[::-1]
        self._orderNodes(ord)
        return ord

    def _orderNodes(self,perm):
        for i in range(len(self)):
            self.X[i]=self.X[i][perm]
            self.Y[i]=self.Y[i][perm]
        self.adj=self.adj[perm][:,perm]
        self.amps = self.amps[perm][:, :, perm]
        # TODO don't need to recompute swap, but couldn't figure out shortcut
        self.swap = adj2swap(self.adj, self.maxDegree)

    def __len__(self):
        return len(self.X)

    def __getitem__(self,idx):
        x, y, adj, amps, swap = self.X[idx], self.Y[idx], self.adj, self.amps, self.swap
        if self.transform:
            x, y, adj, amps, swap = map(self.transform, (x, y, adj, amps, swap))
        return {'x': x, 'y': y, 'adj': adj, 'amps': amps, 'swap': swap}

class multiGraphDataset(Dataset):
    def __init__(self,X,Y,adj,degrees=None,amps=None,swap=None,selfLoops=False,transform=None,pad=False,**kwargs):
        super(multiGraphDataset, self).__init__()
        self.X=X
        self.Y=Y
        self.adj=adj
        if selfLoops:
            for i in range(len(adj)):
                np.fill_diagonal(adj[i],1)
        if degrees:
            self.degrees=degrees
        else:
            self.degrees=[np.int(np.max(np.sum(a,1))) for a in self.adj]
        if amps:
            self.amps=amps
        else:
            self.amps=[adj2amps(a,d) for (a,d) in zip(self.adj,self.degrees)]
        if swap:
            self.swap=swap
        else:
            self.swap=[adj2swap(a,d) for (a,d) in zip(self.adj,self.degrees)]
        self.transform=transform
        self.pad=pad
        if 'padNodes' in kwargs:
            self.maxNodes=kwargs['padNodes']
        else:
            self.maxNodes=np.max([len(x) for x in self.X])
        if "padDegrees" in kwargs:
            self.maxDegree=kwargs['padDegrees']
        else:
            self.maxDegree=np.max(self.degrees)

    def shuffleExamples(self):
        perm=np.random.permutation(len(self))
        self.X, self.Y, self.adj, self.amps, self.swap = \
            list(zip(*[list(zip(self.X, self.Y, self.adj, self.amps, self.swap))[i] for i in perm]))

    def shuffleNodes(self):
        for i in range(len(self)):
            perm=np.random.permutation(len(self.adj[i]))
            self.orderNodes(i,perm)

    def orderNodes(self,ind,perm):
        self.X[ind] = self.X[ind][perm]
        if self.Y[ind].shape[0]==len(perm):
            self.Y[ind] = self.Y[ind][perm]
        self.adj[ind] = self.adj[ind][perm][:, perm]
        self.amps[ind] = self.amps[ind][perm][:, :, perm]
        # TODO don't need to recompute swap, but couldn't figure out shortcut
        self.swap[ind] = adj2swap(self.adj[ind], self.degrees[ind])

    def reorderNodes(self,orderFunc):
        # Order nodes according to given function
        for i in range(len(self)):
            g = nx.from_numpy_matrix(self.adj[i])
            vals = orderFunc(g)
            vals = [int(vals[j]) for j in range(len(self.adj[i]))]
            ord = np.argsort(vals)[::-1]
            self.orderNodes(i,ord)

    def __len__(self):
        return len(self.X)

    def __getitem__(self,idx):
        x, y, adj, amps, swap = self.X[idx], self.Y[idx], self.adj[idx], self.amps[idx], self.swap[idx]
        if self.pad:
            addNodes=self.maxNodes-len(x)
            addDegrees=self.maxDegree-amps.shape[1]
            x=np.pad(x,((0,addNodes),(0,0)),'constant',constant_values=0)
            if y.shape[0]==len(adj):
                y=np.pad(y,((0,addNodes),(0,0)),'constant',constant_values=0)
            adj=np.pad(adj,(0,addNodes),'constant',constant_values=0)
            amps=np.pad(amps,((0,addNodes),(0,addDegrees),(0,addNodes)),'constant',constant_values=0)
            swap=adj2swap(adj,self.maxDegree)
        if self.transform:
            x,y,adj,amps,swap = map(self.transform,(x,y,adj,amps,swap))
        return { 'x' : x, 'y' : y, 'adj' : adj, 'amps' : amps, 'swap' : swap }

def Mutag(file='data/MUTAG.mat'):
    data=sp.io.loadmat(file)
    Y=data['lmutag']
    Y[Y==-1]=0
    Y=list(Y)
    adj,X,_,_=zip(*data['MUTAG'][0])
    adj=list(adj)
    X=[x[0][0][0].flatten() for x in X]
    Xclasses=np.unique(np.hstack(X))
    X=[label2vec(x, Xclasses) for x in X]
    exType="Classification"
    criterion=nn.CrossEntropyLoss()
    evals=[nnUtils.CategoricalAccuracy()]
    return X, Y, adj, exType, criterion, evals

def Enzymes(file='data/ENZYMES.mat'):
    data=sp.io.loadmat(file)
    Y=list(data['lenzymes']-1)
    adj, X, _ = zip(*data['ENZYMES'][0])
    adj=[a.toarray().astype(int) for a in adj]
    X = [x[0][0][0].flatten() for x in X]
    Xclasses = np.unique(np.hstack(X))
    X = [label2vec(x, Xclasses) for x in X]
    exType = "Classification"
    criterion = nn.CrossEntropyLoss()
    evals=[nnUtils.CategoricalAccuracy()]
    return X, Y, adj, exType, criterion, evals

def NCI1(file='data/NCI1.mat'):
    data = sp.io.loadmat(file)
    Y = list(data['lnci1'])
    X, adj , _, _ = zip(*data['NCI1'][0])
    adj = [a.toarray().astype(int) for a in adj]
    X = [x[0][0][0].flatten() for x in X]
    Xclasses = np.unique(np.hstack(X))
    X = [label2vec(x, Xclasses) for x in X]
    exType = "Classification"
    criterion = nn.CrossEntropyLoss()
    evals=[nnUtils.CategoricalAccuracy()]
    return X, Y, adj, exType, criterion, evals
	
def PTC(file='data/PTC.mat'):
    data = sp.io.loadmat(file)
    Y = list(data['lptc'])
    adj, X , _ = zip(*data['ptc'][0])
    adj = [a.astype(int) for a in adj]
    X = [x[0][0][0].flatten() for x in X]
    Xclasses = np.unique(np.hstack(X))
    X = [label2vec(x, Xclasses) for x in X]
    exType = "Classification"
    criterion = nn.CrossEntropyLoss()
    evals=[nnUtils.CategoricalAccuracy()]
    return X, Y, adj, exType, criterion, evals
	
def PROTEINS(file='data/PROTEINS.mat'):
    data = sp.io.loadmat(file)
    Y = list(data['lproteins'])
    adj, X , _= zip(*data['proteins'][0])
    adj = [a.astype(int) for a in adj]
    X = [x[0][0][0].flatten() for x in X]
    Xclasses = np.unique(np.hstack(X))
    X = [label2vec(x, Xclasses) for x in X]
    exType = "Classification"
    criterion = nn.CrossEntropyLoss()
    evals=[nnUtils.CategoricalAccuracy()]
    return X, Y, adj, exType, criterion, evals

def QM7(file='data/qm7.mat'):
    data=sp.io.loadmat(file)
    charges=data['Z'] #7165x23
    coords=data['R'] #7165x23x3
    coulombs = np.array(data['X']) #7165x23x23
    Y=-data['T'].flatten()

    #Remove empty values padding smaller molecules
    molSizes=[np.sum(charge>0) for charge in charges]
    charges,coords,coulombs=list(zip(*[(cha[:s],coo[:s],cou[:s,:s]) for (cha,coo,cou,s)
                                  in zip(charges,coords,coulombs,molSizes)]))

    #For some reason, the Dataset does no give charges in the correct order
    ccharges=[0,36,53,73,388]
    X=[[np.argwhere(c>ccharges).flatten()[-1] for c in coulomb.diagonal()] for coulomb in coulombs]

    # Convert Atomic Charges to 1 hot labels
    Xclasses = np.unique(np.hstack(X))
    X=[label2vec(x,Xclasses) for x in X]

    #Construct Adjacency Matrices
    if os.path.isfile(os.path.join('data', 'QM7Graphs.npy')):
        adj, thresh = np.load(os.path.join('data', 'QM7Graphs.npy'),encoding='latin1')
        adj=[a[:len(x),:len(x)].astype(int) for a,x in zip(adj,X)]
    else:
        #Convert coulomb matrix to similarity matrix
        sims=[coulomb/np.outer(charge,charge) for (coulomb, charge) in zip(coulombs,charges)]
        #Convert similarity matrix into graphs
        adj=[]
        for sim in sims:
            vals=sim[np.triu_indices(sim.shape[0],1)]
            labels=KMeans(2).fit(vals[:,np.newaxis]).labels_
            threshold=np.maximum(np.min(vals[labels==1])),np.min(vals[labels==1])
            graph=(sim>threshold).astype(int)
            np.fill_diagonal(graph,0)
            adj.append(graph)

    #Squash y range
    Ymax=np.max(Y)
    Ymin=np.min(Y)
    range=Ymax-Ymin
    Y=[np.array([(y-Ymin)/(range)],dtype=np.float64) for y in Y]
    exType = "Regression"
    criterion = nn.MSELoss()
    evals = [nnUtils.MSE(range),nnUtils.MAE(range)]
    return X, Y, adj, exType, criterion, evals

def USTemperature(file='data/ustemp/2009.npy',adjfile='data/ustemp/2009adj.npy'):
    records=np.load(file)
    mintemp=np.min(records)
    maxtemp=np.max(records)
    range=maxtemp-mintemp
    records=(records-mintemp)/(maxtemp-mintemp)
    adj=np.load(adjfile)
    X=list(np.float64(records[:-1,:,np.newaxis]))
    Y=list(np.float64(records[1:,:,np.newaxis]))
    exType = "Regression"
    criterion = nn.MSELoss()
    evals = [nnUtils.MSE(range), nnUtils.MAE(range)]
    #evals = [nnUtils.MSE(), nnUtils.MAE()]
    return X, Y, adj, exType, criterion, evals

def loadCora(path="data/cora/",dataset="cora"):
    label2index = {
        'Case_Based': 0,
        'Genetic_Algorithms': 1,
        'Neural_Networks': 2,
        'Probabilistic_Methods': 3,
        'Reinforcement_Learning': 4,
        'Rule_Learning': 5,
        'Theory': 6
    }
    idx_features_labels = np.genfromtxt("{}{}.content".format(path, dataset), dtype=np.dtype(str))
    X=np.array(idx_features_labels[:, 1:-2], dtype=float)
    Y=np.array([label2index[i] for i in idx_features_labels[:, -1]])

    idx = np.array(idx_features_labels[:, 0], dtype=np.int32)
    idx_map = {j: i for i, j in enumerate(idx)}
    edges_unordered = np.genfromtxt("{}{}.cites".format(path, dataset), dtype=np.int32)
    edges = np.array(list(map(idx_map.get, edges_unordered.flatten())),
                     dtype=np.int32).reshape(edges_unordered.shape)

    adj = np.zeros((Y.shape[0], Y.shape[0]))
    for e in edges:
        adj[e[0], e[1]] = 1
        adj[e[1],e[0]]=1
    return X,Y,adj

def loadExperiment(name,**kwargs):
    name=str.lower(name)
    dic={"mutag": Mutag,
         "enzymes": Enzymes,
         "nci1": NCI1,
		 "ptc": PTC,
		 "proteins": PROTEINS,
         "qm7": QM7,
         "ustemp": USTemperature}
    return dic[name](**kwargs)