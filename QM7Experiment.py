import torch.optim as optim
from torch.utils.data import DataLoader

import constructions
from datasets import QM7
from graphLayers import *

"""
This file runs an experiment on the QM7 dataset
"""

class Input(nn.Module):
    def __init__(self,steps=3,theta=1):
        super(Input, self).__init__()
        self.steps=steps
        self.theta=theta

    def expand(self,X):
        mask=(X!=0)
        tx=[]
        for s in np.arange(0,self.steps+1):
            copy=X.clone()
            copy[mask]-=s*self.theta
            tx.append(torch.tanh(copy))
        return torch.cat(tx,2)

    def forward(self,X):
        X=self.expand(X)
        mask=(X!=0)
        X[mask]=(X[mask]-torch.mean(X[mask]))/torch.std(X[mask])
        return X

class Output(nn.Module):
    def __init__(self,std,mean):
        super(Output, self).__init__()
        self.std=Variable(torch.FloatTensor(np.array([std])))
        self.mean=Variable(torch.FloatTensor(np.array([mean])))

    def forward(self,X):
        return (X*self.std)+self.mean

    def cuda(self):
        super(Output, self).cuda()
        self.std=self.std.cuda()
        self.mean=self.mean.cuda()

if __name__=='__main__':
    networks=["qw","dc","kipf"]

    #Experiment Parameters
    network=networks[0]
    batch_size=1
    timesteps=4 #Quantum Walk Length
    denseLayers=[400,100,1]
    finalActivation=nn.Sigmoid
    onGPU=True
    unitary=False
    epochs=20
    embedding=None

    #Set up data
    print("Loading QM7")
    dataset=QM7(swapFunc=constructions.kernelAdj2Swap,reduce=False)
    yMin=np.min(dataset.yLabels)
    yMin=torch.FloatTensor(np.array([yMin]))
    yRange=np.max(dataset.yLabels-np.min(dataset.yLabels))
    yRange=torch.FloatTensor(np.array([yRange]))
    nodes=dataset.adj[0].shape[0]
    degree=dataset.degrees[0]
    features=dataset.xLabels[0].shape[1]


    mae=[]
    testMAE=[]
    testRMSE=[]
    print("Beginning Training")
    for fold in range(5):
        print("Fold "+str(fold+1)+" of 5")
        #Refresh Everything
        testMAE.append([])
        testRMSE.append([])
        dataset.setFold(fold)
        dloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=1)

        #Set up the test data
        testset=dataset.testSet()
        testx=torch.stack([t[0] for t in testset])
        testa=torch.stack([t[1] for t in testset])
        tests=torch.stack([t[2] for t in testset])
        testy=torch.stack([t[3] for t in testset])
        testadj=torch.stack([t[4] for t in testset])
        if onGPU:
            testx = Variable(testx.cuda(), requires_grad=False)
            testa = Variable(testa.cuda(), requires_grad=False)
            testy = (testy - yMin) / yRange  # Flip it and Squash it
            testy = Variable(testy.cuda(), requires_grad=False)
            tests = tests.cuda()
            testadj=Variable(testadj.cuda())
        else:
            testx = Variable(testx, requires_grad=False)
            testa = Variable(testa, requires_grad=False)
            testy = (testy - yMin) / yRange
            testy = Variable(testy, requires_grad=False)
            testadj=Variable(testadj)

        print("Creating Neural Network")
        criterion = nn.MSELoss()
        if network=='qw':
            net = graphNet(nodes, degree, features, [timesteps], unitary=unitary,
                           denseLayers=denseLayers,
                           finalActivation=finalActivation, onGPU=onGPU)
        elif network=='dc':
            net=GCNN(nodes,features,timesteps,
                     graphLayer='dc',
                     denseLayers=denseLayers,
                     finalActivation=finalActivation,
                     onGPU=onGPU)
        elif network=='gat':
            net=GCNN(nodes,features,features,
                     graphLayer='gat',
                     denseLayers=denseLayers,
                     finalActivation=finalActivation,
                     onGPU=onGPU)
        else:
            net=GCNN(nodes,features,features,
                     graphLayer='kipf',
                     denseLayers=denseLayers,
                     finalActivation=finalActivation,
                     onGPU=onGPU)
        opt = optim.Adam(net.parameters())

        for iter in range(epochs):
            running_loss = 0.0
            running_mae = 0.0

            for i_batch, (x,a,s,y,adj) in enumerate(dloader):
                if onGPU:
                    x= Variable(x.cuda(), requires_grad=False)
                    a=Variable(a.cuda(),requires_grad=False)
                    y=(y-yMin)/yRange #Flip it and Squash it
                    y=Variable(y.cuda(),requires_grad=False)
                    s=s.cuda()
                    adj=Variable(adj.cuda(),requires_grad=False)
                else:
                    x= Variable(x, requires_grad=False)
                    a=Variable(a,requires_grad=False)
                    y = (y-yMin) / yRange
                    y=Variable(y, requires_grad=False)
                    adj=Variable(adj,requires_grad=False)

                #Forward and Backward Pass
                opt.zero_grad()  # zero the gradient buffers
                if network=='qw':
                    output=net(x,a,s)
                else:
                    output=net(x,adj)
                loss = criterion(output, y)
                loss.backward()
                opt.step()  # Does the update

                #Statistics
                running_loss += loss.data[0]
                running_mae += (torch.mean(torch.abs(output-y)).data.cpu()*yRange).numpy()[0]
                loss_batches = 200
                if i_batch % loss_batches == loss_batches - 1:  # print every 10 mini-batches
                    print('%5d %5d %.8f %.8f' %
                          (iter + 1, i_batch + 1, running_loss / loss_batches, running_mae / loss_batches))
                    running_loss = 0.0
                    mae.append(running_mae / loss_batches)
                    running_mae = 0.0

            #Evaluate Test Set
            if network=='qw':
                testout = net(testx,testa,tests)
            else:
                testout = net(testx,testadj)
            diff = (torch.abs(testout-testy).data.cpu()*yRange).numpy()
            testMAE[-1].append(np.mean(diff))
            testRMSE[-1].append(np.sqrt(np.mean(diff**2)))
            print("Test MAE "+str(testMAE[-1][-1]))
            print("Test RMSE"+str(testRMSE[-1][-1]))
            np.save(network+'_QM7_testMAE_sim',testMAE)
            np.save(network+'_QM7_testRMSE_sim',testRMSE)