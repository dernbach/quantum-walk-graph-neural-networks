import torch.optim as optim
from torch.utils.data import DataLoader

import constructions
from datasets import nme
from graphLayers import *
from nnUtils import categoricalAccuracy

"""
This file runs an experiment on the mutag, enzymes, or nci1 datasets
"""

def prepBatch(x, a, s, y, adj, onGPU):
    if onGPU:
        x = Variable(x.cuda(), requires_grad=False)
        a = Variable(a.cuda(), requires_grad=False)
        y = Variable(y.long().cuda(), requires_grad=False)
        s = s.cuda()
        adj = Variable(adj.cuda(), requires_grad=False)
    else:
        x = Variable(x, requires_grad=False)
        a = Variable(a, requires_grad=False)
        y = Variable(y.long(), requires_grad=False)
        adj = Variable(adj, requires_grad=False)
    return x, a, s, y, adj

if __name__=='__main__':
    files=['mutag','enzymes','nci1']
    NNs=['QW','DC','GC']

    #Choose dataset and Experiment Parameters
    file=files[1]
    NN='QW'
    batch_size=1
    timesteps=2 #QW length or DC hops
    onGPU=False
    criterion=nn.CrossEntropyLoss()
    epochs=10
    unitary=True
    nodeLayers=None
    nodeSummary=set2vec#graphEncoder
    denseLayers=[]
    finalActivation=nn.Softmax
    swapFunc=constructions.kernelAdj2Swap#constructions.adj2swap#constructions.kernelAdj2Swap
    optimizer=optim.Adam
    embedding=None#3


    #Load in data
    print("Loading "+str(file))
    dataset=nme(name=file,swapFunc=swapFunc)
    sample=dataset.__getitem__(0)
    nodes=sample[0].shape[0]
    degree=np.max(dataset.degrees)
    features=sample[0].shape[1]
    outclasses=np.max(dataset.Y)+1
    dloader=DataLoader(dataset, batch_size=batch_size,shuffle=True,num_workers=1)

    acc=[]
    valAcc=[]
    valLoss=[]
    testAcc=[]
    print("Beginning Training")
    for tests in range(5):
        if NN=='QW':
            net = graphNet(nodes, degree, features, timesteps,
                           unitary=unitary,
                           nodeLayers=nodeLayers,
                           nodeSummary=nodeSummary,
                           denseLayers=denseLayers+[outclasses],
                           finalActivation=finalActivation,
                           embedding=embedding,
                           onGPU=onGPU)
        elif NN=='DC':
            net = GCNN(nodes, features, timesteps,
                       graphLayer='dc',
                       denseLayers=[outclasses],
                       finalActivation=finalActivation,
                       onGPU=onGPU)
        elif NN=='GC':
            net = GCNN(nodes, features, nodeLayers,
                       graphLayer='kipf',
                       denseLayers=[outclasses],
                       finalActivation=finalActivation,
                       onGPU=onGPU)
        elif NN=='GAT':
            net = GCNN(nodes,features,nodeLayers,
                       graphLayer='gat',
                       denseLayers=[outclasses],
                       finalActivation=finalActivation,
                       onGPU=onGPU)
        else:
            print("Neural Network Type Not Understood")
            quit()

        testAcc.append([])
        valAcc.append([])
        valLoss.append([])
        opt = optimizer(net.parameters()) #optim.Adam(net.parameters())
        dataset.shuffle()
        print("Trial "+str(tests+1))
        for iter in range(epochs):
            running_loss = 0.0
            running_acc = 0.0
            for i_batch, (x,a,s,y,adj) in enumerate(dloader):
                x,a,s,y,adj=prepBatch(x,a,s,y,adj,onGPU)

                opt.zero_grad()  # zero the gradient buffers
                if NN=='QW':
                    output=net(x,a,s)
                else:
                    output=net(x,adj)
                loss = criterion(output, y)
                loss.backward()
                opt.step()  # Does the update

                running_acc += categoricalAccuracy(output, y).data.numpy()
                running_loss += loss.item()
                loss_batches = 25
                if i_batch % loss_batches == loss_batches - 1:  # print every 10 mini-batches
                    print('%5d %5d %.8f %.8f'%
                          (iter + 1, i_batch + 1, running_loss / loss_batches, running_acc / loss_batches))
                    running_loss = 0.0
                    acc.append(running_acc)
                    running_acc = 0.0

            #Validation Time
            (x,a,s,y,adj)=dataset.valSet()
            x, a, s, y, adj = prepBatch(x, a, s, torch.from_numpy(y), adj, onGPU)
            if NN=='QW':
                output=net(x,a,s)
            else:
                output = net(x, adj)

            vLoss=criterion(output,y).data.cpu().numpy()
            valLoss[-1].append(vLoss)
            vAcc=categoricalAccuracy(output, y).data.numpy()
            print("Validation Accuracy: "+str(vAcc))
            valAcc[-1].append(vAcc)

            #Test Time
            (x,a,s,y,adj)=dataset.testSet()
            x, a, s, y, adj = prepBatch(x, a, s, torch.from_numpy(y), adj, onGPU)
            if NN=='QW':
                output=net(x,a,s)
            else:
                output = net(x, adj)
            tAcc=categoricalAccuracy(output, y).data.numpy()
            print("Test Accuracy: "+str(tAcc))
            testAcc[-1].append(tAcc)
        np.save(NN+'_'+file+'_valacc',valAcc)
        np.save(NN+'_'+file+"_testacc",testAcc)
    s=[testAcc[i][np.argmin(valLoss[i])] for i in range(5)]
    t=[testAcc[i][np.argmax(valAcc[i])] for i in range(5)]
    print(str(np.mean(s))+" "+str(np.std(s)))
    print(str(np.mean(t))+" "+str(np.std(t)))