import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.nn import init
from torch.nn.modules.linear import Linear

from coins import groverDiffusion
from nnUtils import linearActivation

"""
This File contains torch.nn.Modules that can take in different graphs at runtime
It has implementations of Quantum Walk Neural Networks, Kipf's and Welling's graph convolutional networks,
and Atwood and Towsley's Diffusion Convolution networks
"""

class relaxedQW(nn.Module):
    """
    A Quantum Walk Neural Network Layer with no restrictions on the operators
    """
    def __init__(self,nodes,degree,features,time_steps=1,**kwargs):
        super(relaxedQW, self).__init__()
        self.nodes=nodes
        self.degree=degree
        self.time_steps=time_steps
        self.features=features

        #Create Coins
        self.coins = nn.ParameterList()
        for t in range(self.time_steps):
            self.coins.append(nn.Parameter(
                torch.FloatTensor(groverDiffusion(self.degree))))
                #nn.init.uniform(torch.FloatTensor(self.degree,self.degree),-1,1)))

    def forward(self,x,init_amps,swap):
        """
        Outputs diffused features according to the quantum walk
        :param x: Feature Tensor, size=(batch,nodes,features)
        :param init_amps: Amplitude Tensor, size=(batch,noes,degree,walkers)
        :param swap: Swap Tensor, size=(batch,2,nodes*degree)
        :return: Diffused matrix z
        """
        amps = init_amps
        for t in range(self.time_steps):
            #Coin Operator
            a=torch.matmul(amps.permute(0,1,3,2),self.coins[t]).permute(0,1,3,2)

            #Swap Operator: The loop is a workaround to allow for permuting elements without destroying the gradient
            app = []
            for i in range(amps.size()[0]):
                ai=a[i]
                app.append(ai[swap[i,0],swap[i,1]].view((1,)+init_amps.size()[1:]))
            amps=torch.cat(app,0)
        d=torch.sum(amps*amps,dim=2)
        out=torch.matmul(torch.transpose(d,1,2),x)
        return out

    def getWalk(self,init_amps,swap):
        """
        Outputs diffused features according to the quantum walk
        :param x: Feature Tensor, size=(batch,nodes,features)
        :param init_amps: Amplitude Tensor, size=(batch,noes,degree,walkers)
        :param swap: Swap Tensor, size=(batch,2,nodes*degree)
        :return: Diffused matrix z
        """
        amps = init_amps
        for t in range(self.time_steps):
            #Coin Operator
            a=torch.matmul(amps.permute(0,1,3,2),self.coins[t]).permute(0,1,3,2)

            #Swap Operator: The loop is a workaround to allow for permuting elements without destroying the gradient
            app = []
            for i in range(amps.size()[0]):
                ai=a[i]
                app.append(ai[swap[i,0],swap[i,1]].view((1,)+init_amps.size()[1:]))
            amps=torch.cat(app,0)
        return torch.sum(amps*amps,dim=2)

    def outShape(self,x=None):
        return (self.nodes,self.features)

class unitaryQW(nn.Module):
    """
    A Quantum Walk Neural Network Layer that restricts the operators to be unitary
    """
    def __init__(self,nodes,degree,features,time_steps=1,addbias=False,**kwargs):
        super(unitaryQW, self).__init__()
        self.nodes=nodes
        self.degree=degree
        self.time_steps=time_steps
        self.features=features
        self.addbias=addbias

        #Create Coins
        #TODO: There may be some errors with specific degree sizes, for now change degree if its a problem
        self.capacity=np.int(np.ceil(np.log2(self.degree))) #Adjustable
        self.thetaA = nn.ParameterList()
        self.thetaB = nn.ParameterList()
        for t in range(self.time_steps):
            self.thetaA.append(nn.Parameter(nn.init.uniform_(torch.FloatTensor(int(self.degree/2),int(self.capacity/2)),-1,1)))
            self.thetaB.append(nn.Parameter(nn.init.uniform_(torch.FloatTensor(int(self.degree/2-1),int(self.capacity/2)),-1,1)))
        if self.addbias:
            self.bias=nn.Parameter(nn.init.uniform_(torch.FloatTensor(self.features*self.time_steps)))

    def _EUNN(self,x,thetaA,thetaB):
        """
        This method adapted from https://github.com/jingli9111/URNN-PyTorch
        See "Tunable Efficient Unitary Neural Networks...", Jing et al.
        :param x: size=(batch,nodes,degree,walkers)
        :param thetaA: Rotation angles, size=(degree/2,capacity/2)
        :param thetaB: Rotation angles, size=(degree/2-1,capacity/2)
        :return: The rotated version of x
        """
        L=self.capacity
        N=self.degree
        sinA = torch.sin(thetaA)
        cosA = torch.cos(thetaA)
        sinB = torch.sin(thetaB)
        cosB = torch.cos(thetaB)

        I = Variable(torch.ones((int(L / 2), 1))).to(x.device)
        O = Variable(torch.zeros((int(L / 2), 1))).to(x.device)

        diagA = torch.stack((cosA, cosA), 2)
        offA = torch.stack((-sinA, sinA), 2)
        diagB = torch.stack((cosB, cosB), 2)
        offB = torch.stack((-sinB, sinB), 2)

        diagA = diagA.view(int(L / 2), N)
        offA = offA.view(int(L / 2), N)
        diagB = diagB.view(int(L / 2), N - 2)
        offB = offB.view(int(L / 2), N - 2)

        diagB = torch.cat([I, diagB, I], 1)
        offB = torch.cat([O, offB, O], 1)

        batch_size = x.size()[0]
        nodes = x.size()[1]
        walkers = x.size()[3]
        x = x.permute(0,1,3,2) #Batch Edit
        #x = (Batch, Nodes, Walkers, degree)
        for i in range(int(L / 2)):
            #A
            y = x.contiguous().view(batch_size, nodes, walkers, int(N / 2), 2)
            y = torch.stack((y[:,:, :, :, 1], y[:, :, :, :, 0]), 4)
            y = y.view(batch_size, nodes, walkers, N)

            x = torch.mul(x, diagA[i])
            y = torch.mul(y, offA[i])

            x = x + y

            # B
            x_top = x[:, :, :, 0]
            x_mid = x[:, :, :, 1:-1].contiguous()
            x_bot = x[:, :, :, -1]
            y = x_mid.view(batch_size, nodes, walkers, int(N / 2 - 1), 2)
            y = torch.stack((y[:, :, :, :, 1], y[:, :, :, :, 0]), 4)
            y = y.view(batch_size, nodes, walkers, N - 2)
            x_top = torch.unsqueeze(x_top, 3)
            x_bot = torch.unsqueeze(x_bot, 3)
            y = torch.cat((x_top, y, x_bot), 3)

            x = x * diagB[i].expand(batch_size, nodes, walkers, N)
            y = y * offB[i].expand(batch_size, nodes, walkers, N)

            x = x + y
        return x.permute(0,1,3,2)

    def forward(self,x,init_amps,swap):
        """
        Outputs diffused features according to the quantum walk
        :param x: Feature Tensor, size=(batch,nodes,features)
        :param init_amps: Amplitude Tensor, size=(batch,nodes,degree,walkers)
        :param swap: Swap Tensor, size=(batch,2,nodes*degree)
        :return: Diffused matrix z
        """
        amps = init_amps
        z=[]
        for t in range(self.time_steps):
            #Coin Operator
            a=self._EUNN(amps,self.thetaA[t],self.thetaB[t])

            # Swap Operator: The loop is a workaround to allow for permuting elements without destroying the gradient
            app = []
            for i in range(amps.size()[0]):
                ai=a[i]
                app.append(ai[swap[i,0],swap[i,1]].view((1,)+init_amps.size()[1:]))#Batch edit
            amps=torch.cat(app,0)

        d=torch.sum(amps*amps,dim=2)
        out=torch.matmul(torch.transpose(d,1,2),x)
        if self.addbias:
            return out+self.bias
        return out

    def getWalk(self,init_amps,swap):
        amps = init_amps
        for t in range(self.time_steps):
            #Coin Operator
            a=self._EUNN(amps,self.thetaA[t],self.thetaB[t])

            # Swap Operator: The loop is a workaround to allow for permuting elements without destroying the gradient
            app = []
            for i in range(amps.size()[0]):
                ai=a[i]
                app.append(ai[swap[i,0],swap[i,1]].view((1,)+init_amps.size()[1:]))#Batch edit
            amps=torch.cat(app,0)

        return torch.sum(amps*amps,dim=2)

    def outShape(self,x=None):
        return (self.nodes,self.features)

class piggyBank(nn.Module):
    """
    Makes Coins out of features
    """
    def __init__(self,fin,fout,unitary=True):
        """
        :param fin: input features size
        :param fout: output coin size fout x fout
        :param unitary:  If true, the coin is returned is unitary (note: comp. expensive)
        :param normalize: If true, normalizes the columns of the coin when unitary is False
        """
        super(piggyBank, self).__init__()
        self.fin=fin
        self.fout=fout
        self.unitary=unitary
        self.W=nn.Parameter(init.uniform_(torch.DoubleTensor(fin,fout),-1,1))
        self.register_buffer("I",torch.eye(int(fout)))

    def forward(self,X):
        """
        :param X: A batch x nodes x features tensor
        :return: A batch x nodes x fout x fout tensor
        """
        A=torch.matmul(X,self.W)
        V=torch.matmul(A.unsqueeze(-1),A.unsqueeze(-2))
        if self.unitary:
            Vscale = torch.matmul(A.unsqueeze(-2), A.unsqueeze(-1))
            Vscale[Vscale==0]=1
            return self.I - 2 * V / Vscale
        return V

    def outShape(self,x=None):
        if x is not None:
            return (x.shape[1],self.fout,self.fout)
        return (self.nodes,self.fout,self.fout)

class eqPiggyBank(nn.Module):
    """
    Creates equivariant coins
    """
    def __init__(self,features,neighbors,unitary=True):
        super(eqPiggyBank,self).__init__()
        self.f=features
        self.W=nn.Parameter(init.uniform_(torch.DoubleTensor(self.f,self.f),-1,1))
        self.unitary=unitary
        self.neighbors=neighbors
        self.register_buffer("I", torch.eye(int(self.neighbors)))

    def forward(self,X,Neighbors):
        N=Neighbors.shape[-1]/self.f
        left=torch.tensordot(X,self.W,1)
        sims=torch.sum(left.unsqueeze(-2)*Neighbors,-1)
        V = torch.matmul(sims.unsqueeze(-1), sims.unsqueeze(-2))
        if self.unitary:
            Vscale = torch.matmul(sims.unsqueeze(-2), sims.unsqueeze(-1))
            Vscale[Vscale==0]=1
            return self.I - 2 * V / Vscale
        return V

    def outShape(self,x):
        return None

class funcQW(nn.Module):
    """
    A Quantum Walk Neural Network Layer that restricts the operators to be unitary
    """
    def __init__(self,nodes,degree,features,time_steps=1,eq=False,**kwargs):
        """
        :param nodes: Nodes in the graph
        :param degree: Maximum degree of the graph
        :param features: Number of features used to construct a coin
        :param time_steps: Number of steps to perform the walk
        :param kwargs: others
        """
        super(funcQW, self).__init__()
        self.nodes=int(nodes)
        self.degree=int(degree)
        self.time_steps=int(time_steps)
        self.features=int(features)
        self.eq=eq
        self.Bank=nn.ModuleList()
        if eq:
            for t in range(self.time_steps):
                self.Bank.append(eqPiggyBank(self.features,self.degree,unitary=True))
        else:
            for t in range(self.time_steps):
                self.Bank.append(piggyBank(self.features * self.degree, self.degree))

    def collectNeighbors(self, x, swap):
        """
        Concatanates the feature vectors of the neighbors of x
        A node's own features are used to fill if neighbors<maxdegree
        :param x: Feature tensor ((batch) x nodes x features)
        :param swap: Swap tensor ((batch) x 2 x nodes*degree)
        :return: Concatenated tensor  ((batch) x nodex x degree x features)
        """
        #nodes = x.shape[-2]
        #N=swap.shape[-1]/nodes
        inds = swap[..., 0, :]

        if x.dim() == 3:
            y = []
            for z, s in zip(x, inds):
                y.append(z[s].reshape(self.nodes, self.degree, self.features))
            return torch.stack(y, 0)
        return x[inds].reshape(self.nodes, self.degree, self.features)

    def forward(self,x,init_amps,swap):
        """
        Outputs diffused features according to the quantum walk
        :param x: Feature Tensor, size=(batch,nodes,features)
        :param coins: size(nodes,degree,degree)
        :param init_amps: Amplitude Tensor, size=(batch,nodes,degree,walkers)
        :param swap: Swap Tensor, size=(batch,2,nodes*degree)
        :param xc: Feature Tensor for Coin, size=(batch,nodes,features), None sets xc=x
        :return: Diffused matrix z
        """
        amps = init_amps

        d=torch.sum(init_amps*init_amps,dim=2)
        out=x
        all_out=out

        neighbors = self.collectNeighbors(out, swap)
        for t in range(self.time_steps):
            #Bank
            if self.eq:
                coins=self.Bank[t](x,neighbors)
            else:
                coins = self.Bank[t](neighbors.reshape(neighbors.shape[0],neighbors.shape[1],-1))

            #Coin Operator
            a=torch.matmul(amps.permute(0,1,3,2),coins).permute(0,1,3,2)

            # Swap Operator: The loop is a workaround to allow for permuting elements without destroying the gradient
            app = []
            for i in range(amps.size()[0]):
                ai=a[i]
                app.append(ai[swap[i,0],swap[i,1]].view((1,)+init_amps.size()[1:]))#Batch edit
            amps=torch.cat(app,0)

            #Diffusion
            d=torch.sum(amps*amps,dim=2)
            out=torch.matmul(torch.transpose(d,1,2),x)
            all_out = torch.cat((all_out, out), dim=2)
        return all_out

    def outShape(self,x=None):
        return (self.nodes,self.features*(self.time_steps+1))

class lstmEncoder(nn.Module):
    def __init__(self,features,hiddensize):
        super(lstmEncoder, self).__init__()
        self.hiddensize=hiddensize
        self.features=features
        self.lstm=nn.LSTM(self.features,self.hiddensize,batch_first=True,bidirectional=True)

    def forward(self,x):
        out, (h, c) = self.lstm(x)
        return out[:,-1]

#Adapted from https://github.com/brain-research/mpnn/
class zeroLSTM(nn.Module):
    def __init__(self,h_size,attention=False):

        m_size=h_size
        if attention:
            m_size=2*h_size

        super(zeroLSTM, self).__init__()
        #Input Gate
        self.im=nn.Parameter(torch.empty((m_size,h_size),dtype=torch.float64,))
        nn.init.uniform_(self.im,-1,1)
        self.ib=nn.Parameter(torch.zeros((1,h_size),dtype=torch.float64))

        #Forget Gate
        self.fm=nn.Parameter(torch.empty((m_size,h_size),dtype=torch.float64))
        nn.init.uniform_(self.fm, -1, 1)
        self.fb = nn.Parameter(torch.zeros((1, h_size), dtype=torch.float64))

        #Cell
        self.cm = nn.Parameter(torch.empty((m_size, h_size),dtype=torch.float64))
        nn.init.uniform_(self.cm, -1, 1)
        self.cb = nn.Parameter(torch.zeros((1, h_size), dtype=torch.float64))

        #Output Gate
        self.om = nn.Parameter(torch.empty((m_size, h_size),dtype=torch.float64))
        nn.init.uniform_(self.om, -1, 1)
        self.ob = nn.Parameter(torch.zeros((1, h_size), dtype=torch.float64))

    def forward(self,h_in,c_in):
        #Input Gate
        ig=torch.sigmoid(torch.matmul(h_in,self.im)+self.ib)

        #Forget Gate
        fg=torch.sigmoid(torch.matmul(h_in,self.fm)+self.fb)

        #Cell
        cprime=torch.sigmoid(torch.matmul(h_in,self.cm)+self.cb)
        c=fg*c_in+ig*torch.tanh(cprime)

        #Output Gate
        og=torch.sigmoid(torch.matmul(h_in,self.om)+self.ob)

        h=og*torch.tanh(c)
        return h,c

#Adapted from https://github.com/brain-research/mpnn/
class set2vec(nn.Module):
    def __init__(self,features,steps,outSize=None):
        super(set2vec, self).__init__()
        self.features=features
        self.out_dim=steps
        self.outSize = features #if outSize is None else outSize
        self.lstm=zeroLSTM(self.outSize,attention=True)
        self.att_w2=nn.Parameter(init.uniform_(torch.empty(self.outSize,features),-1,1))
        self.att_v=nn.Parameter(init.uniform_(torch.empty(features,1),-1,1))

    def forward(self,x):
        (batch_size,nodes,features) = x.shape
        h = torch.zeros((batch_size, 2*features), dtype=torch.float64, device=x.device)
        c = torch.zeros((batch_size, features), dtype=torch.float64, device=x.device)


        logit_att = []

        for i in range(self.out_dim):
            h,c = self.lstm(h,c)
            query = torch.matmul(h,self.att_w2).reshape(-1,1,features)
            energies = torch.matmul(torch.tanh(query+x),self.att_v)
            att=F.softmax(energies,1)

            read=torch.sum(x*att,1)
            h=torch.cat((h,read),1)

            logit_att.append(h)
        return h

    def outShape(self,x=None):
        return (2*self.outSize,)

class concatNodes(nn.Module):
    def forward(self,x):
        return x.view(x.size()[0],-1)

    def outShape(self,x=None):
        if x is None:
            return (-1,)
        elif type(x) is tuple:
            return (x[-2]*x[-1],)
        return (x.shape[-2]*x.shape[-1],)

class graphNet(nn.Module):
    def __init__(self, nodes, degree, features, glType, glSizes, embedding=None, dropout=0.0,
                 nodeLayers=None, nodeSummary=None, denseLayers=None, activations=None, finalActivation=None, **kwargs):
        super(graphNet, self).__init__()
        self.nodes=nodes
        self.degree=degree
        self.features=features
        self.graphLayerName=glType
        self.graphLayers=nn.ModuleList()
        self.nodeLayers=nn.ModuleList()
        self.denseLayers=nn.ModuleList()
        self.activations=nn.ModuleList()
        self.dropout=nn.Dropout(p=dropout)

        #Embeddings convert one hot node feature vectors into dense vectors
        if embedding is not None:
            self.embedding=nn.Linear(features,embedding)
            currentShape=(nodes,embedding)
        else:
            self.embedding=None
            currentShape = (nodes, features)

        #Construct the graph layers
        if isinstance(glSizes,int):
            glSizes=[glSizes]
        for s in glSizes:
            name = str.lower(glType)
            layers = {"funcqw": funcQW,
                      "eqfuncqw": funcQW,
                      "relaxedqw": relaxedQW,
                      "unitaryqw": unitaryQW,
                      "gcn": GCN,
                      "dcnn": DCNN,
                      "gat": GAT}
            if name in ["funcqw", "relaxedqw", "unitaryqw"]:
                self.graphLayers.append(layers[name](self.nodes, self.degree, currentShape[-1], s))
            elif name == "eqfuncqw":
                self.graphLayers.append(layers[name](self.nodes,self.degree,currentShape[-1],s,eq=True))
            elif name in ["gcn", "gat","dcnn"]:
                self.graphLayers.append(layers[name]( currentShape[-1], s))
            currentShape = self.graphLayers[-1].outShape(currentShape)

        #Node Layers act on each node's features independently of other nodes
        if nodeLayers is not None:
            for nL in nodeLayers:
                self.nodeLayers.append(Linear(currentShape[-1],nL))
                currentShape=(self.nodes,nL)

        #Node Summary is a function over nodes that reduces a node x features matrix to a summary feature vector
        nodeSummary=str.lower(nodeSummary)
        summaries={"cat":concatNodes,
                   "lstm":lstmEncoder,
                   "ave":NodeAve,
                   "max":NodeMax,
                   "s2v":set2vec}
        if nodeSummary in ["cat","ave","max"]:
            self.nodeSummary = summaries[nodeSummary]()
            currentShape = self.nodeSummary.outShape(currentShape)
        elif nodeSummary in ["lstm"]:
            self.nodeSummary = summaries[nodeSummary](currentShape[1],currentShape[1])
            currentShape = self.nodeSummary.outShape(currentShape)
        elif nodeSummary in ["s2v"]:
            self.nodeSummary = summaries[nodeSummary](currentShape[1],12)
            currentShape = self.nodeSummary.outShape(currentShape)
        else:
            self.nodeSummary=None

        #Dense Layers act on the summarized graph or combine features across nodes
        if denseLayers is not None:
            for dL in denseLayers:
                self.denseLayers.append(Linear(currentShape[-1],dL))
                currentShape=(dL,)

        layerCount= len(self.graphLayers) + len(self.nodeLayers) + len(self.denseLayers)
        if activations is None:
            self.activations.extend([linearActivation()] * (layerCount - 1))
        elif isinstance(type(activations), nn.Module):
            self.activations.extend([activations()] * (layerCount - 1))
        else:
            self.activations.extend([a() for a in activations])

        if finalActivation is None:
            self.activations.append(linearActivation())
        else:
            self.activations.append(finalActivation())

    def forward(self,x,**kwargs):
        #init_amps=init_amps.float()
        #swap=swap.long()
        act=0
        if self.embedding is not None:
            x=self.embedding(x)
        for wL in self.graphLayers:
            x=wL(x,**kwargs)
            x=self.activations[act](x)
            act+=1
        x=self.dropout(x)
        for nL in self.nodeLayers:
            x=nL(x)
            x=self.activations[act](x)
            act+=1
        if self.nodeSummary:
            x = self.nodeSummary(x)
        for dL in self.denseLayers:
            x=dL(x)
            x=self.activations[act](x)
            act+=1
        return x

class NodeAve(nn.Module):
    def __init__(self):
        super(NodeAve, self).__init__()
    def forward(self,X):
        sum=torch.sum(X,1)
        q=(X!=0).sum(1).float()
        q[q==0]=1
        return sum/q

    def outShape(self,x):
        if x is None:
            return (-1,)
        if type(x) is tuple:
            return (x[-1],)
        return x.shape[-1:]

class NodeMax(nn.Module):
    def __init__(self):
        super(NodeMax, self).__init__()
    def forward(self,X):
        return torch.max(X,1)[0]

    def outShape(self,x):
        if x is None:
            return (-1,)
        if type(x) is tuple:
            return (x[-1],)
        return x.shape[-1:]

class GCN(nn.Module):
    #"Semi-Supervised Classification with Graph Convolutional Networks." Kipf, Thomas N. and Welling, Max
    def __init__(self,fin,fout):
        super(GCN, self).__init__()
        self.fin=fin
        self.fout=fout
        self.W=Linear(fin,fout)
        self.W=self.W.double()

    def forward(self, X, adj):
        """
        Performs a forward pass of the graph convolution layer
        :param X: Feature Tensor, size=(batch,nodes,f_in)
        :param adj: Adjacency Matrices, size=(batch,nodes,features)
        :return: Diffused weighted features y=W \tilde{A} X, size=(batch,nodes,f_out)
        """
        if X.is_cuda:
            Atil= adj.data + torch.eye(adj.size()[1]).cuda()
            D=Atil.sum(2)
            Dsr=[]
            for mat in D:
                Dsr.append(torch.diag(torch.ones(mat.shape).cuda()/mat.sqrt()))
            Ds=torch.stack(Dsr)
            DAD = Variable(Ds.matmul(Atil).matmul(Ds).cuda())
        else:
            Atil= adj.data + torch.eye(adj[0].size()[0])
            D=Atil.sum(2)
            Dsr=[]
            for mat in D:
                Dsr.append(torch.diag(torch.ones(mat.shape,dtype=torch.double)/mat.sqrt()))
            Ds=torch.stack(Dsr)
            DAD=Variable(Ds.matmul(Atil).matmul(Ds))
        return self.W(DAD.matmul(X))

    def outShape(self,x=None):
        if x is None:
            return (-1,self.fout)
        if type(x) is tuple:
            return (x[-2],self.fout)
        return (x.shape[-2],self.fout)

class DCNN(nn.Module):
    """
    See "Diffusion Convolution Neural Networks." Atwood, James and Towsley, Don
    """
    def __init__(self,fin,hops):
        """
        :param fin: Input features per node
        :param hops: Number of powers of adjacency matrix
        """
        super(DCNN, self).__init__()
        self.fin=fin
        self.hops=hops
        self.W=nn.Parameter(torch.DoubleTensor(fin*(hops+1)))
        init.uniform_(self.W,-0.1,0.1)

    def forward(self, X, adj):
        """
        Performs a forward pass of the Diffusion Convolution Layer
        :param X: Feature Tensor, size=(batch,nodes,f_in)
        :param adj: Adjacency Matrices, size=(batch,nodes,nodes)
        :return: Diffused feature tensor, size=(batch,nodes,f_in,hops)
        """
        if X.is_cuda:
            Atil = adj.data
            D = Atil.sum(2)+1
            anorm=Atil/D.expand(Atil.shape[2],D.shape[0],D.shape[1]).permute(1,2,0) #works to her
            Apow=torch.eye(anorm.shape[1]).expand_as(Atil).cuda()
            out=[]
            out.append(Variable(Apow).matmul(X))
            for i in range(self.hops):
                Apow=Atil.matmul(Apow)
                out.append(Variable(Apow).matmul(X))
            out=torch.cat(out,-1)
        else:
            Atil = adj.data
            D = Atil.sum(2)+1
            anorm=Atil/D.expand(Atil.shape[2],D.shape[0],D.shape[1]).permute(1,2,0)
            Apow=torch.eye(anorm.shape[1]).expand_as(Atil)
            out=[]
            out.append(Variable(Apow).matmul(X))
            for i in range(self.hops):
                Apow=Atil.matmul(Apow)
                out.append(Variable(Apow).matmul(X))
            out=torch.stack(out,-1)
        return out.reshape(out.shape[0],out.shape[1],-1)*self.W.view(1,1,-1)

    def outShape(self, x=None):
        if x is None:
            return (-1, self.fin*(self.hops+1))
        if type(x) is tuple:
            return (x[-2], self.fin*(self.hops+1))
        return (x.shape[-2], self.fin*(self.hops+1))

class GAT(nn.Module):
    """
    Simple GAT layer, similar to https://arxiv.org/abs/1710.10903
    """

    def __init__(self, in_features, out_features, dropout=0, alpha=0.01, concat=True):
        super(GAT, self).__init__()
        self.dropout = dropout
        self.in_features = in_features
        self.out_features = out_features
        self.alpha = alpha
        self.concat = concat

        self.W = nn.Parameter(nn.init.xavier_uniform(torch.Tensor(in_features, out_features).type(torch.DoubleTensor),
                                                     gain=np.sqrt(2.0)), requires_grad=True)
        self.a = nn.Parameter(nn.init.xavier_uniform(torch.Tensor(2*out_features, 1).type(torch.DoubleTensor),
                                                     gain=np.sqrt(2.0)), requires_grad=True)

        self.leakyrelu = nn.LeakyReLU(self.alpha)

    def forward(self, X, adj):
        h = torch.matmul(X, self.W)
        B=h.size()[0]
        N = h.size()[1]
        a_input = torch.cat([h.repeat(1, 1, N).view(B, N * N, -1), h.repeat(1, N, 1)], dim=1).view(B,N, -1, 2 * self.out_features)
        e = self.leakyrelu(torch.matmul(a_input, self.a).squeeze(3))

        zero_vec = -9e15*torch.ones_like(e)
        attention = torch.where(adj > 0, e, zero_vec)
        attention = F.softmax(attention, dim=2)
        attention = F.dropout(attention, self.dropout, training=self.training)
        h_prime = torch.matmul(attention, h)

        if self.concat:
            return F.elu(h_prime)
        else:
            return h_prime

    def outShape(self, x=None):
        if x is None:
            return (-1, self.out_features)
        if type(x) is tuple:
            return (x[-2], self.out_features)
        return (x.shape[-2], self.out_features)

    def __repr__(self):
        return self.__class__.__name__ + ' (' + str(self.in_features) + ' -> ' + str(self.out_features) + ')'





















