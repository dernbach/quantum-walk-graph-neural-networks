from datetime import datetime

import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.data import Subset

import graphLayers
import staticLayers
from datasets import *
from nnUtils import *
from utils import getkfold, createSplits

torch.set_default_dtype(torch.float64)

if __name__=='__main__':
    #Edit this to change general experiment parameters
    params = {"dataset": "ustemp", #Name of experiment (see datasets.loadExperiment)
              "trials": 5, #Number of times to run the experiment
              "train_split": 0.34, #Percent of samples for training, the remaining is split equally between val and test
              #If an integer, does k-fold validation
              "batch_size": 1, #number of samples to batch together
              "epochs": 32, #Iterations of the training set per experiment
              "force_cpu": False, #Forces the experiment to run on cpu even if cuda available
              "printLoss": 10, #Prints training loss every so many minibatches.
              "selfLoops":False, #Modifies graphs to have self loops on each node
              "orderNodes": None, #centrality measure to order nodes (None,"betweenness")
              "shuffle_nodes":False} #Shuffle Nodes every epoch

    #Edit this to change neural network, do not delete params, only change their values
    nnParams = {"glType": "dcnn", #Graph Layer Type
                "glSizes":[5], #Sizes of graph layers (walk length, out features, etc.)
                "embedding":None, #Used to embed input node features (int)
                "unitary":False, #Constrain the coin operator to be unitary (Bool)
                "dropout":0.0, #Ratio of data to dropout (float between 0 and 1)
                "nodeLayers":[], #Linear Layers that work at the node level (None, int)
                "nodeSummary":"None",#'s2v', #Summarizes nodes if graph level output ('cat','lstm','set2vec',None)
                "denseLayers":[], #Linear Layers that work after the node summary (None, list)
                "activations":None, #Activation functions after each layer (None, single type, list of types)
                "reshape":False, #If True adds a final denseLayer to match the output shape of the experiment
                "finalActivation":None}

    trialTime=datetime.now().strftime("%Y-%m-%d-%H-%M")

    # Load Data from Files
    X, Y, adj, exType, criterion, evalMetrics = loadExperiment(params["dataset"])
    nnParams["features"]=X[0].shape[1]

    #Determine experiment Type
    if exType == 'Classification':
        outShape=len(np.unique(np.hstack(Y)))
        Y=[y.astype(np.int64) for y in Y]
    elif exType == 'Regression':
        outShape = Y[0].shape[-1]
        #if nnParams["finalActivation"] is None:
         #   nnParams["finalActivation"]=nn.Sigmoid

    #Determine Single or multigraph task
    if type(adj) is list:
        layers=graphLayers
        dSet = multiGraphDataset(X, Y, adj, selfLoops=params['selfLoops'], transform=torch.from_numpy, pad=True)
        nnParams["nodes"]=dSet.maxNodes
        nnParams["degree"]=dSet.maxDegree
    else:

        layers=staticLayers
        dSet = graphDataset(X,Y,adj,transform=torch.from_numpy)
        nnParams['adj']=dSet.adj
        #Quick fix
        """
        layers=graphLayers
        dSet = multiGraphDataset(X, Y, [adj]*len(X), selfLoops=params['selfLoops'], transform=torch.from_numpy, pad=True)
        nnParams["nodes"] = dSet.maxNodes
        nnParams["degree"] = dSet.maxDegree
        """
    if nnParams["reshape"]:
        nnParams["denseLayers"]=nnParams["denseLayers"]+[outShape] if nnParams["denseLayers"] else [outShape]

    #Reorder Nodes
    if params["orderNodes"]=="betweeness":
        dSet.reorderNodes(nx.betweenness_centrality)
        params["shuffle_nodes"]=False

    #Set up cuda/cpu
    if params['force_cpu']:
        device=torch.device('cpu')
    else:
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("Running on:",device)

    #Prep k-fold
    if params['train_split']>1:
        kperm=np.random.permutation(len(dSet))

    valScores,testScores = (np.zeros((params["trials"],params["epochs"],len(evalMetrics)+1)) for i in range(2))
    for trial in range(params["trials"]):
        print('Beginning Trial {0}'.format(trial+1))
        print("Epoch Minibatch Loss "+" ".join([str(ev) for ev in evalMetrics]))

        #Split Data into train and test sets
        if params['train_split']>1:
            splits=getkfold(params['train_split'],trial,kperm)
        else:
            splits=createSplits(len(dSet),params["train_split"])
        trainSet,valSet,testSet = (Subset(dSet,ids) for ids in splits)
        trainLoader, valLoader, testLoader = (DataLoader(s,batch_size=params['batch_size'],shuffle=True,num_workers=1)
                                              for s in (trainSet,valSet,testSet))

        #Build Neural Network
        net = layers.graphNet(**nnParams)
        net=net.to(device)
        opt = optim.Adam(net.parameters())

        for iter in range(params['epochs']):
            if params["shuffle_nodes"]:
                dSet.shuffleNodes()
            runningLoss = 0.0
            runningAcc = 0.0

            #Training
            samples,batches=0,0
            vals=np.zeros(len(evalMetrics),dtype=np.float32)
            for minibatch in trainLoader:
                x,y,adj,amps,swap=map(lambda x:x.to(device),(
                    minibatch['x'],minibatch['y'],minibatch['adj'],minibatch['amps'],minibatch['swap']))
                miniBatchSize=x.shape[0]
                samples+=x.shape[0]
                batches+=1
                opt.zero_grad()
                if "qw" in nnParams["glType"]:
                    out=net(x,init_amps=amps,swap=swap.long())
                else:
                    out=net(x,adj=adj.double())
                loss=criterion(out.squeeze(1),y.squeeze(1))
                loss.backward()
                opt.step()
                vals = vals + np.array([ev(out,y).item() for ev in evalMetrics])*miniBatchSize
                #runningAcc += secondaryCriterion(out, y).item() * miniBatchSize
                runningLoss += loss.item() #* miniBatchSize
                if batches % params["printLoss"] == 0:
                    print("{:3d} {:3d} {:.8f} ".format(iter+1,batches,runningLoss/samples)
                          +" ".join(["{:.8f}".format(v.item()) for v in vals/samples]))

            #Validation
            vLoss=0.0
            vals = np.zeros(len(evalMetrics), dtype=np.float32)
            with torch.set_grad_enabled(False):
                for minibatch in valLoader:
                    x, y, adj, amps, swap = map(lambda x: x.to(device), (
                        minibatch['x'], minibatch['y'], minibatch['adj'], minibatch['amps'], minibatch['swap']))
                    miniBatchSize=x.shape[0]
                    if "qw" in nnParams["glType"]:
                        out = net(x, init_amps=amps, swap=swap.long())
                    else:
                        out = net(x, adj=adj.double())
                    vLoss+=criterion(out.squeeze(1),y.squeeze(1)).item() * miniBatchSize
                    vals = vals + np.array([ev(out,y) for ev in evalMetrics])*miniBatchSize
                vLoss=vLoss/len(valSet)
                vals=vals/len(valSet)
                print('Validation Loss: %.8f'%(vLoss))
                for i in range(len(evalMetrics)):
                    print("Vailidation {}: {:.8f}".format(evalMetrics[i],vals[i]))
                valScores[trial,iter,0]=vLoss
                valScores[trial,iter,1:]=vals

            # Test
            testLoss = 0.0
            vals = np.zeros(len(evalMetrics), dtype=np.float32)
            with torch.set_grad_enabled(False):
                for minibatch in testLoader:
                    x, y, adj, amps, swap = map(lambda x: x.to(device), (
                        minibatch['x'], minibatch['y'], minibatch['adj'], minibatch['amps'], minibatch['swap']))
                    miniBatchSize = x.shape[0]
                    if "qw" in nnParams["glType"]:
                        out = net(x, init_amps=amps, swap=swap.long())
                    else:
                        out = net(x, adj=adj.double())
                    testLoss += criterion(out.squeeze(1), y.squeeze(1)).item() * miniBatchSize
                    vals = vals + np.array([ev(out,y) for ev in evalMetrics])*miniBatchSize
                testLoss = testLoss / len(testSet)
                vals = vals / len(testSet)
                print('Test Loss: %.8f' % (testLoss / len(testSet)))
                for i in range(len(evalMetrics)):
                    print("Test {}: {:.8f}".format(evalMetrics[i], vals[i]))
                testScores[trial, iter, 0] = testLoss
                testScores[trial, iter, 1:] = vals
                print("")
            np.save("results/"+params["dataset"]+"_"+nnParams["glType"]+"_"+trialTime,{
                "params":{k:str(v) for k,v in params.items()},
                "nnParams":{k:str(v) for k,v in nnParams.items()},
                "network":str(net),
                "valScores":valScores,
                "testScores":testScores})