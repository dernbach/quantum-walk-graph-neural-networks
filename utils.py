import numpy as np
import scipy.sparse as sp


def label2vec(labels,lset=None):
    if lset is None:
        lset=np.unique(labels)
    out=np.zeros((len(labels),len(lset)))
    for i in range(len(labels)):
        out[i,np.argwhere(labels[i]==lset).flatten()[0]]=1
    return out

def encode_onehot(labels):
    classes = set(labels)
    classes_dict = {c: np.identity(len(classes))[i, :] for i, c in enumerate(classes)}
    labels_onehot = np.array(list(map(classes_dict.get, labels)), dtype=np.int32)
    return labels_onehot

def sample_mask(idx, l):
    mask = np.zeros(l)
    mask[idx] = 1
    return np.array(mask, dtype=np.bool)

def get_splits(y):
    idx_train = range(140)
    idx_val = range(200, 500)
    idx_test = range(500, 1500)
    y_train = np.zeros(y.shape, dtype=np.int32)
    y_val = np.zeros(y.shape, dtype=np.int32)
    y_test = np.zeros(y.shape, dtype=np.int32)
    y_train[idx_train] = y[idx_train]
    y_val[idx_val] = y[idx_val]
    y_test[idx_test] = y[idx_test]
    train_mask = sample_mask(idx_train, y.shape[0])
    return y_train, y_val, y_test, idx_train, idx_val, idx_test, train_mask

def categorical_crossentropy(preds, labels):
    return np.mean(-np.log(np.extract(labels, preds)))

def accuracy(preds, labels):
    return np.mean(np.equal(np.argmax(labels, 1), np.argmax(preds, 1)))

def evaluate_preds(preds, labels, indices):

    split_loss = list()
    split_acc = list()

    for y_split, idx_split in zip(labels, indices):
        split_loss.append(categorical_crossentropy(preds[idx_split], y_split[idx_split]))
        split_acc.append(accuracy(preds[idx_split], y_split[idx_split]))

    return split_loss, split_acc

def chebyshev_polynomial(X, k):
    """Calculate Chebyshev polynomials up to order k. Return a list of sparse matrices."""
    print("Calculating Chebyshev polynomials up to order ",k)

    T_k = list()
    T_k.append(sp.eye(X.shape[0]).tocsr())
    T_k.append(X)

    def chebyshev_recurrence(T_k_minus_one, T_k_minus_two, X):
        X_ = sp.csr_matrix(X, copy=True)
        return 2 * X_.dot(T_k_minus_one) - T_k_minus_two

    for i in range(2, k+1):
        T_k.append(chebyshev_recurrence(T_k[-1], T_k[-2], X))

    return T_k

def sparse_to_tuple(sparse_mx):
    if not sp.isspmatrix_coo(sparse_mx):
        sparse_mx = sparse_mx.tocoo()
    coords = np.vstack((sparse_mx.row, sparse_mx.col)).transpose()
    values = sparse_mx.data
    shape = sparse_mx.shape
    return coords, values, shape

def adj_mat2list(adj):
    return [np.argwhere(a==1).flatten() for a in adj]

def reorderLists(newOrder,*enums):
    return (list(e) for e in zip(*[list(zip(*enums))[i] for i in newOrder]))

def createSplits(samples,train_split):
        perm=np.random.permutation(samples)
        split1=int(samples*train_split)
        split2=split1+int((samples-split1)/2)
        return perm[:split1],perm[split1:split2],perm[split2:]

def getkfold(k,fold,items):
    numitems=len(items)
    setsize=numitems/k

    splits=np.array([items[int(i*setsize):int((i+1)*setsize)] for i in range(k)])

    if fold==0:
        return np.concatenate(splits[1:k-1]),splits[k-1],splits[0]
    else:
        train_inds = list(range(k))
        testset = splits[train_inds.pop(fold)]
        valset = splits[train_inds.pop(fold-1)]
        return np.concatenate(splits[train_inds]),valset,testset

def getkfolds(k,fold,items):
    num_items=len(items)

    inds=np.arange(len(num_items))
    if fold==0:
        val=inds[inds%k==k-1]
    else:
        val=inds[inds%k==fold-1]
    test=inds[inds%k==fold]

