import torch
import torch.nn as nn

class linearActivation(nn.Module):
    def __init__(self):
        super(linearActivation, self).__init__()
    def forward(self,x):
        return x

def meanAbsoluteError(y_pred,y):
    return (torch.mean(torch.abs(y_pred - y)))

class CategoricalAccuracy():
    def __str__(self):
        return "Acc"

    def __call__(self,y_pred,y):
        indices = torch.argmax(y_pred, 1)
        correct = torch.eq(indices, y.view(indices.shape))
        return torch.mean(correct.float())

class MAE():
    def __init__(self,scale=1.0):
        self.scale=scale

    def __str__(self):
        return "MAE"

    def __call__(self,y_pred,y):
        err = y_pred - y
        return self.scale * torch.mean(torch.abs(err))

class MSE():
    def __init__(self,scale=1.0):
        self.scale=scale

    def __str__(self):
        return "MSE"

    def __call__(self,y_pred,y):
        err = self.scale*(y_pred - y)
        return torch.mean(err * err)