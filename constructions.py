import collections

import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg.eigen.arpack import eigsh, ArpackNoConvergence


def createTorusShift(dim=1,w=3,splitDims=False):
    """
    Creates a the shift tensor for a lattice of given specifications.
    :param dim: Dimension of the hypertorus (i.e. 1=circle, 2=donut)
    :param w: Scaler or enumerable designating widths along each dimension. Scaler implies equiwidth dimensions.
    :param splitDims: For a 2d-torus, returns a (w[0],w[1],d,w[0],w[1],d) dimensional tensor instead of (w[0]w[1],d,w[0]w[1],d).
    :return: A 4(6) dimensional tensor
    """
    if not isinstance(w, collections.Iterable):
        w=[w]*dim
    if dim == 1:
        shift = np.zeros((w[0], 2 * dim, w[0], 2 * dim), dtype=float)
        for i in range(w[0]):
            shift[i, [0, 1], [(i + 1) % w[0], i - 1], [0, 1]] = [1, 1]
    elif dim == 2 and splitDims: #Useful for plotting
        # Shift Operator. If pos(i,j) and pos(k,l) are adjacent add a directional vector to the 2nd and 5th dimensions
        shift = np.zeros((w[0], w[1], 2 * dim, w[0], w[1], 2 * dim), dtype=float)
        for i in range(w[0]):
            for j in range(w[1]):
                shift[i, j, [0, 1, 2, 3],
                    [(i + 1) % w[0], i - 1, i, i],
                    [j, j, (j + 1) % w[1], j - 1], [0, 1, 2, 3]] =[1, 1, 1, 1]
    else:
        N = np.prod(w)
        d = 2 * dim
        shift = np.zeros((N, d, N, d), dtype=float)
        for j in np.arange(dim,dtype=int):
            offset = np.prod(w[:j], dtype=int)
            mod = np.prod(w[:j + 1], dtype=int)
            for i in np.arange(N,dtype=int):
                shift[i, 2*j, (i + offset) % mod + (i / mod) * mod, 2*j] = 1
                shift[i, 2*j+1, (i - offset) % mod + (i / mod) * mod, 2*j+1] = 1
    return shift

def createTorusAdj(dim=1,w=3):
    """
    Creates an adjacency matrix for a torus with the given parameters
    :param dim: Number of dimensions of the hypertorus
    :param w: Scaler or enumerable designating widths along each dimension. Scaler implies equiwidth dimensions.
    :return: N x N adjacency matrix where N=w[0]*w[1]*..*w[dim-1]
    """
    if not isinstance(w, collections.Iterable):
        w=[w]*dim
    N=np.prod(w)
    d = 2 * dim
    adj = np.zeros((N,N), dtype=float)
    for j in np.arange(dim,dtype=int):
        offset = np.prod(w[:j], dtype=int)
        mod = np.prod(w[:j + 1], dtype=int)
        for i in np.arange(N,dtype=int):
            adj[i, (i + offset) % mod + (i / mod) * mod] = 1
            adj[i, (i - offset) % mod + (i / mod) * mod] = 1
    return adj

def shiftToAdj(shift):
    """
    Converts a shift tensor to an adjacency matrix
    :param shift: size=(nodes, degrees, nodes, degrees)
    :return: adj matrix, size=(nodes,nodes)
    """
    return np.sum(np.sum(shift,np.ndim(shift)/2-1),-1)

def adjToSwapTensor(adj,sparse=False):
    """
    Constructs a tensor operator that swaps 1 direction of amplitude for adjacent nodes
    :param adj: An n x n adjaceny matrix
    :return: An n x d x n x d tensor, where d is the max degree of the graph
    """
    n=adj.shape[0]
    d=np.int(np.max(np.sum(adj,axis=1)))
    if sparse:
        swap=sp.coo_matrix((n,d,n,d),dtype=int)
    else:
        swap=np.zeros((n,d,n,d))
    ind=np.zeros(n,dtype=int)

    for i in range(n):
        for j in np.arange(i+1,n):
            if adj[i,j]==1:
                swap[i,ind[i],j,ind[j]]=1
                swap[j,ind[j],i,ind[i]]=1
                ind[i]+=1
                ind[j]+=1
    return swap

def rWDKernel(adj, k=3, addSelfLoop=True):
    """
    Returns a matrix of node kernels that are the dot product of the walker distributions
    at the kth step of a random walk beginning at each node respectively
    :param adj: 1-0 Adj matrix of the graph
    :param k: Length of Random Walk
    :param addSelfLoop: Adds a loop at each node, helps with increasing neighbors products especially in teh case of 
    near-bipartite graphs
    :return: Returns an n x n symmetric matrix
    """
    if addSelfLoop:
        adj=adj+np.eye(adj.shape[0])
    d = np.sum(adj, 1).astype(float)
    rwAdj = (adj / d).T
    rwAdj_k = np.array(rwAdj)
    for i in range(k - 1):
        rwAdj_k = rwAdj_k.dot(rwAdj)
    return rwAdj_k.dot(rwAdj_k.T)

def kernelAdj2Swap(adj, d=None, kernel=None):
    n=adj.shape[0]
    if d is None:
        d=np.int(np.max(np.sum(adj,axis=1)))

    if kernel is None:
        kernel = rWDKernel

    #Determine Node Similarities
    sims = rWDKernel(adj)
    #Sort every node according to its similarity with each node
    inds=np.argsort(sims,axis=1)[:,::-1]
    #Find the ranking of each node according to above sorting
    order=np.argsort(inds)

    #Put each neighbor of each node in a sorted list
    neighbors=[]
    for i in range(n):
        temp=np.argwhere(adj[i]==1).flatten()
        neighbors.append(temp[np.argsort(order[i,temp])])

    #Create the Swap lists
    a,b=[],[]
    for i in range(n):
        for j in range(d):
            if j<len(neighbors[i]):
                node=neighbors[i][j]
                a.append(node)
                b.append(np.argwhere(neighbors[node]==i)[0,0])
            else:
                a.append(i)
                b.append(j)

    swap=np.stack([a,b],axis=0)
    return swap

def createRandomRingShift(nodes):
    """
    Creates the shift tensor for a random ring graph with w nodes
    :param nodes: Number of nodes in graph
    :return: Shift tensor, size=(nodes,2,nodes,2)
    """
    shift=np.zeros((nodes, 2, nodes, 2), dtype=float)
    for i in np.arange(nodes, dtype=int):
        a=np.random.permutation([0,1])
        shift[i, a, [(i + 1) % nodes, i - 1], a] = [1, 1]
    return shift

def adj2list(adj,conversion=None):
    """
    Converts an adjacency matrix to an adjacency list
    :param adj: adjacency matrix, size=(nodes,nodes)
    :param conversion: A function to apply to the adjacent vertices
    :return: list of length nodes, elements of length neighbors
    """
    adj_list=[]
    for i in range(len(adj)):
        inds=np.where(adj[i]==1)[0]
        if conversion is not None:
            adj_list.append(conversion(inds))
        else:
            adj_list.append(inds)
    return adj_list

def adj2amps(adj,degree=None):
    """
    Creates an amplitude matrix from an adjacency matrix
    :param adj: adjacency matrix, size=(nodes,nodes)
    :param degree: size of max degree (or larger)
    :return: amplitude tensor, size=(nodes,degree,nodes)
    """
    size=len(adj)
    if degree is None:
        degree=np.int(np.max(np.sum(adj,1)))
    amps=np.zeros((size,degree,size))
    for i in range(size):
        idegree=np.int(np.sum(adj[i]))
        if idegree==0:
            continue
        amps[i,:idegree, i ]=1./np.sqrt(idegree)
    return amps

def adj2swap(adj,degree=None):
    """
    Converts an adjacency matrix to a swap vectors
    :param adj: adjacency matrix, size=(nodes,nodes)
    :param degree: max degree (or larger)
    :return: swap vectors, size=(2,nodes*degree)
    """
    size=len(adj)
    if degree is None:
        degree=np.int(np.max(np.sum(adj,1)))
    # Convert Adjacency Matrices to Swap Tensors
    swap=np.zeros((2, int(size * degree)), dtype=int)
    a, b = [], []
    inds = np.zeros(size)
    for j in range(size):
        neighbors = np.argwhere(adj[j] == 1).flatten()
        for n in range(degree):
            if n < len(neighbors):
                a.append(neighbors[n])
                b.append(inds[neighbors[n]])
                inds[neighbors[n]] += 1
            else:
                a.append(j)
                b.append(n)
    swap[0] = a
    swap[1] = b
    return swap

def rescale_laplacian(laplacian):
    try:
        largest_eigval = eigsh(laplacian, 1, which='LM', return_eigenvectors=False)[0]
    except ArpackNoConvergence:
        print('Eigenvalue calculation did not converge! Using largest_eigval=2 instead.')
        largest_eigval = 2

    scaled_laplacian = (2. / largest_eigval) * laplacian - sp.eye(laplacian.shape[0])
    return scaled_laplacian
