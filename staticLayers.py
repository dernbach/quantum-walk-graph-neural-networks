import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.nn import init
from torch.nn.modules.linear import Linear

from coins import groverDiffusion
from constructions import adj2swap
from graphLayers import lstmEncoder, concatNodes, set2vec
from nnUtils import linearActivation

"""
This file contains torch.nn.Modules that require the graph to be defined at the time of layer creation.
It has implementations of Quantum Walk Neural Networks, Kipf's and Welling's graph convolutional networks,
and Atwood and Towsley's Diffusion Convolution networks. These are much older than the graphLayers versions.
"""

class spatialQWNN(nn.Module):
    """
    A Quantum Walk Layer with a unique coin per node
    """
    def __init__(self, adj, walkLength=2, uniform=False, features=1, **kwargs):
        """
        A Quantum Walk Neural Network LAyer
        :param adj: Adjacency Matrix, size=(nodes,nodes)
        :param walkLength: Length of the quantum walk
        :param uniform: Constrain coins to be uniform
        :param kwargs: nn.Module keywords
        """
        super(spatialQWNN, self).__init__()
        self.nodes=len(adj)
        self.degrees=[np.sum(a) for a in adj]
        self.maxDegree=np.max(self.degrees)
        self.time_steps=walkLength
        self.adj=adj
        self.edges=np.sum(self.degrees)/2
        self.uniform=uniform
        self.features = features


        #Create Initial Amplitudes of walkers, size=(nodes, spins, walkers)
        init_amps=np.zeros((self.nodes,self.maxDegree,self.nodes))
        for i in range(self.nodes):
            init_amps[i,:self.degrees[i],i]=1./np.sqrt(self.degrees[i])
        self.init_amps=nn.Parameter(torch.DoubleTensor(init_amps))

        #Create Swap Tensor, size=(2,nodes*degrees)
        swap=adj2swap(self.adj,self.maxDegree)
        self.register_buffer('swap',torch.LongTensor(swap))

        #Create Coins
        self.coins=nn.ParameterList()
        for d in self.degrees:
            self.coins.append(nn.Parameter((torch.DoubleTensor(groverDiffusion(d)))))

    def forward(self,x):
        """
        Performs a forward pass of the Quantum Walk LAyer
        :param x: Feature Tensor, size=(batch,nodes,features)
        :return: Diffused Feature Tensor, size=(batch,nodes,features)
        """
        amps = self.init_amps
        for t in range(self.time_steps):
            a=torch.DoubleTensor(self.init_amps.size()).zero_().to(x.device)
            for i in range(len(self.coins)):
                #Coin Operator
                r=torch.matmul(self.coins[i],amps[i,:self.degrees[i]])
                #Shift Operator
                a[i,:self.degrees[i]]=r
            amps=a[self.swap[0],self.swap[1]]
            amps=amps.view(self.init_amps.size())
        d = torch.sum(amps * amps, dim=1)
        return torch.matmul(torch.transpose(d,0,1),x)

    def getWalk(self):
        amps = self.init_amps
        ampslist=[amps.data]
        walkmatrix=[torch.sum(amps * amps, dim=1).float()]
        for t in range(self.time_steps):
            a = torch.FloatTensor(self.init_amps.size()).zero_().to(self.init_amps.device)
            for i in range(len(self.coins)):
                # Coin Operator
                r = torch.matmul(self.coins[i], amps[i, :self.degrees[i]])
                # Shift Operator
                a[i, :self.degrees[i]] = r
            amps = a[self.swap[0], self.swap[1]]
            amps = amps.view(self.init_amps.size())
            ampslist.append(amps.data)
            walkmatrix.append(torch.sum(amps*amps,dim=1).data)
        return ampslist,walkmatrix

    def outShape(self,x=None):
        return (self.nodes,self.features)

class temporalQWNN(nn.Module):
    """
    A quantum walk layer with a single unique coin per time step
    """
    def __init__(self,adj,num_walkers=None,learn_coin=True,
                 learn_amps=False,onGPU=False,time_steps=1,
                 **kwargs):
        """
        A Quantum Walk Neural Network Layer
        :param adj: Adjacency Matrix, size=(nodes,nodes)
        :param num_walkers: Number of walkers to create the diffusion matrix, None->nodes
        :param learn_coin: Use the grover diffusion operator or allow the coin to be learned
        :param learn_amps: Allow the initial amplitudes of the walkers to be learned
        :param onGPU: Perform operations on the gpu
        :param time_steps: Length of the quantum walk
        :param kwargs: nn.Module keywords
        """
        super(temporalQWNN, self).__init__()
        self.nodes=len(adj)
        self.degrees=[len(a) for a in adj]
        self.maxdegree=np.max(self.degrees)
        self.time_steps=time_steps
        self.adj=adj #list of LongTensor of ind
        self.edges=np.sum(self.degrees)
        self.learn_amps=learn_amps
        self.learn_coin=learn_coin

        if num_walkers is None:
            self.walkers=self.nodes
        else:
            self.walkers=num_walkers

        #amp dimensions are [nodes, spins, walkers]
        init_amps=np.zeros((self.nodes,self.maxdegree,self.walkers))
        for i in range(self.nodes):
            #Sart Walkers across all nodes when fewer walkers than nodes
            if self.walkers<self.nodes:
                init_amps[i,:self.degrees[i]] = np.random.rand(self.degrees[i],self.walkers)*2-1
            else:
                init_amps[i,:self.degrees[i],i]=1./np.sqrt(self.degrees[i])
        if self.walkers<self.nodes:
            for w in range(self.walkers):
                init_amps[:,:,w]=init_amps[:,:,w]/np.linalg.norm(init_amps[:,:,w])
        if self.learn_amps:
            self.init_amps=nn.Parameter(torch.from_numpy(init_amps))
        else:
            self.init_amps=Variable(torch.from_numpy(init_amps),requires_grad=False)

        #Create Swap Tensor
        swap=np.zeros((2,self.nodes*self.maxdegree),dtype=int)
        a,b=[],[]
        inds=np.zeros(self.nodes)
        for i in range(len(adj)):
            for n in range(self.maxdegree):
                if n<adj[i].size()[0]:
                    node=adj[i][n]
                    a.append(node)
                    b.append(inds[node])
                    inds[node]+=1
                else:
                    a.append(i)
                    b.append(n)
        swap[0]=a
        swap[1]=b
        self.swap=torch.from_numpy(swap)

        #Create Coins
        if self.learn_coin:
            self.coins = nn.ParameterList()
            for t in range(self.time_steps):
                self.coins.append(nn.Parameter(torch.from_numpy(
                    groverDiffusion(self.maxdegree))))
        else:
            self.coins=[Variable(torch.from_numpy(
                groverDiffusion(self.maxdegree),
                requires_grad=False)) for t in range(self.time_steps)]


        self.onGPU=onGPU
        if self.onGPU:
            self.toGPU()

    def forward(self,x):
        """
        Performs a forward pass of the Quantum Walk LAyer
        :param x: Feature Tensor, size=(batch,nodes,features)
        :return: Diffused Feature Tensor, size=(batch,nodes,features)
        """
        amps = self.init_amps
        for t in range(self.time_steps):
            #Coin Operator
            a=torch.matmul(self.coins[t],amps)
            #Swap Operator
            amps=a[self.swap[0],self.swap[1]].view(self.init_amps.size())
        d1=torch.sum(amps*amps,dim=1)
        d2=torch.sum(self.init_amps*self.init_amps,dim=1)
        self.D=torch.matmul(d2,torch.transpose(d1,0,1))
        return torch.matmul(torch.transpose(self.D,0,1),x)

    def toGPU(self):
        """
        Moves the module to the GPU
        """
        self.onGPU=True
        self.cuda()
        self.swap=self.swap.cuda()
        if not self.learn_amps:
            self.init_amps=self.init_amps.cuda()
        if not self.learn_coin:
            for t in range(self.time_steps):
                self.coins[t]=self.coins[t].cuda()

class unitaryTemporalQWNN(nn.Module):
    def __init__(self,adj,num_walkers=None,learn_amps=False,learn_coin=True,onGPU=False,time_steps=1,
                 **kwargs):
        """
        :param adj: Adjacency Matrix, size=(nodes,nodes)
        :param num_walkers: Number of walkers to create the diffusion matrix, None->nodes
        :param learn_coin: Use the grover diffusion operator or allow the coin to be learned
        :param learn_amps: Allow the initial amplitudes of the walkers to be learned
        :param onGPU: Perform operations on the gpu
        :param time_steps: Length of the quantum walk
        :param kwargs: nn.Module keywords
        """
        super(unitaryTemporalQWNN, self).__init__()
        self.nodes=len(adj)
        self.degrees=[len(a) for a in adj]
        self.maxdegree=np.max(self.degrees)
        self.time_steps=time_steps
        self.adj=adj #list of LongTensor of ind
        self.edges=np.sum(self.degrees)
        self.learn_amps=learn_amps
        self.learn_coin=learn_coin


        if num_walkers is None:
            self.walkers=self.nodes
        else:
            self.walkers=num_walkers

        #amp dimensions are [nodes, spins, walkers]
        init_amps=np.zeros((self.nodes,self.maxdegree,self.walkers))
        for i in range(self.nodes):
            #Sart Walkers across all nodes when fewer walkers than nodes
            if self.walkers<self.nodes:
                init_amps[i,:self.degrees[i]] = np.random.rand(self.degrees[i],self.walkers)*2-1
            else:
                init_amps[i,:self.degrees[i],i]=1./np.sqrt(self.degrees[i])
        if self.walkers<self.nodes:
            for w in range(self.walkers):
                init_amps[:,:,w]=init_amps[:,:,w]/np.linalg.norm(init_amps[:,:,w])
        if self.learn_amps:
            self.init_amps=nn.Parameter(torch.from_numpy(init_amps).float())
        else:
            self.init_amps=Variable(torch.from_numpy(init_amps).float(),requires_grad=False)

        #Create Swap Tensor
        swap=np.zeros((2,self.nodes*self.maxdegree),dtype=int)
        a,b=[],[]
        inds=np.zeros(self.nodes)
        for i in range(len(adj)):
            for n in range(self.maxdegree):
                if n<adj[i].size()[0]:
                    node=adj[i][n]
                    a.append(node)
                    b.append(inds[node])
                    inds[node]+=1
                else:
                    a.append(i)
                    b.append(n)
        swap[0]=a
        swap[1]=b
        self.swap=torch.from_numpy(swap)

        #Create Coins
        self.capacity=np.int(np.ceil(np.log2(self.maxdegree))) #Adjustable
        self.thetaA = nn.ParameterList()
        self.thetaB = nn.ParameterList()
        for t in range(self.time_steps):
            self.thetaA.append(nn.Parameter(torch.FloatTensor(self.maxdegree/2,self.capacity/2)))
            self.thetaB.append(nn.Parameter(torch.FloatTensor(self.maxdegree/2-1,self.capacity/2)))

        self.reset_parameters()

        self.onGPU=onGPU
        if self.onGPU:
            self.toGPU()

    def reset_parameters(self):
        for i in range(self.time_steps):
            init.uniform(self.thetaA[i],a=-0.1,b=0.1)
            init.uniform(self.thetaB[i],a=-0.1,b=0.1)

    def _EUNN(self,x,thetaA,thetaB):
        """
        This method adapted from https://github.com/jingli9111/URNN-PyTorch
        See "Tunable Efficient Unitary Neural Networks...", Jing et al.
        :param x: size=(nodes,degree,walkers)
        :param thetaA: Rotation angles, size=(degree/2,capacity/2)
        :param thetaB: Rotation angles, size=(degree/2-1,capacity/2)
        :return: The rotated version of x
        """
        L=self.capacity
        N=self.maxdegree
        sinA = torch.sin(thetaA)
        cosA = torch.cos(thetaA)
        sinB = torch.sin(thetaB)
        cosB = torch.cos(thetaB)

        I = Variable(torch.ones((L / 2, 1)))
        O = Variable(torch.zeros((L / 2, 1)))

        if self.onGPU:
            I=I.cuda()
            O=O.cuda()

        diagA = torch.stack((cosA, cosA), 2)
        offA = torch.stack((-sinA, sinA), 2)
        diagB = torch.stack((cosB, cosB), 2)
        offB = torch.stack((-sinB, sinB), 2)

        diagA = diagA.view(L / 2, N)
        offA = offA.view(L / 2, N)
        diagB = diagB.view(L / 2, N - 2)
        offB = offB.view(L / 2, N - 2)

        diagB = torch.cat([I, diagB, I], 1)
        offB = torch.cat([O, offB, O], 1)

        batch_size = x.size()[0]
        x = x.permute(0,2,1)
        for i in range(L / 2):
            #A
            y = x.contiguous().view(batch_size, batch_size, N / 2, 2)
            y = torch.stack((y[:, :, :, 1], y[:, :, :, 0]), 3)
            y = y.view(batch_size, batch_size, N)

            x = torch.mul(x.float(), diagA[i])
            y = torch.mul(y.float(), offA[i])

            x = x + y

            # B
            x_top = x[:, :, 0]
            x_mid = x[:, :, 1:-1].contiguous()
            x_bot = x[:, :, -1]
            y = x_mid.view(batch_size, batch_size, N / 2 - 1, 2)
            y = torch.stack((y[:, :, :, 1], y[:, :, :, 0]), 3)
            y = y.view(batch_size, batch_size, N - 2)
            x_top = torch.unsqueeze(x_top, 2)
            x_bot = torch.unsqueeze(x_bot, 2)
            y = torch.cat((x_top, y, x_bot), 2)

            x = x * diagB[i].expand(batch_size, batch_size, N)
            y = y * offB[i].expand(batch_size, batch_size, N)

            x = x + y
        return x.permute(0,2,1)

    def forward(self,x):
        """
        Performs a forward pass of the quantum walk layer
        :param x: Feature Tensor, size=(batch,nodes,features)
        :return: Diffused Feature Tensor, size=(batch,nodes,features)
        """
        amps = self.init_amps
        for t in range(self.time_steps):
            #Coin Operator
            a=self._EUNN(amps,self.thetaA[t],self.thetaB[t])
            #Swap Operator
            amps=a[self.swap[0],self.swap[1]].view(self.init_amps.size())
        d=torch.sum(amps*amps,dim=1)
        z= torch.matmul(torch.transpose(d,1),x)
        return z

    def toGPU(self):
        """
        Moves the module to the GPU
        """
        self.onGPU=True
        self.cuda()
        self.swap=self.swap.cuda()
        if not self.learn_amps:
            self.init_amps=self.init_amps.cuda()

class DCNN(nn.Module):
    """
    See "Diffusion Convolution Neural Networks." Atwood, James and Towsley, Don
    """
    def __init__(self,features,hops,adj=None,addBias=True,**kwargs):
        """
        :param features: Features per node
        :param hops: Number of powers of the adjacency matrix
        :param adj: Adjacency array, size=(nodes,nodes)
        :param addBias: Add a bias to each hop/feature combination
        :param nonlinearity: Applies the given nonlinearity to the output (ie nn.Sigmoid)
        :param w: Initial weight matrix, size=(1,hops+1,features)
        :param sumHops: Reduce the output dimensions by summing over the hops dimension
        :param onGPU: Move the module to the GPU
        :param kwargs: torch.nn.Module keywords
        """
        super(DCNN, self).__init__()

        self.features=features
        self.hops=hops
        self.nodes=adj.shape[0]

        D = np.maximum(adj.sum(1),1)
        anorm = torch.DoubleTensor(adj / D)  # works to her
        aStack=torch.DoubleTensor(hops+1,self.nodes,self.nodes)
        aStack[0]=torch.eye(self.nodes)
        for i in range(hops):
            aStack[i+1]=torch.matmul(aStack[i],anorm)
        self.register_buffer("A",aStack)

        self.W=nn.Parameter(torch.DoubleTensor(self.hops + 1, self.features))
        init.uniform_(self.W, -0.1, 0.1)

        self.addBias=addBias
        if self.addBias:
            self.b=nn.Parameter(torch.zeros(self.hops+1,self.features))


    def forward(self,x):
        """
        Performs a forward pass of the diffusion convolution layer
        :param x: Feature Tensor, size=(batch,nodes,features)
        :return: Diffused Feature Tensor, size=(batch,nodes,hops,features) or if sumHops (batch,nodes,features)
        """
        y=torch.tensordot(x,self.A,([-2],[1])).permute(0,3,2,1)
        y=y*self.W

        if self.addBias:
            y=y+self.b
        return y.reshape((y.shape[0],self.nodes,-1))

    def outShape(self,x):
        if x is None:
            return (-1,self.nodes,(self.hops+1)*self.features)
        elif type(x) is tuple:
            return (x[-2],self.nodes,(self.hops+1)*x[-1])
        return (x.shape[-2],self.nodes,(self.hops+1)*x[-1])

class spectralLayer(nn.Module):
    """
    See: "Spectral Networks and Locally Connected Networks on Graphs." Bruna, Joan et al.
    """
    def __init__(self,U,addBias=True,nonlinearity=None,W=None,ongpu=False):
        """
        :param U: k Eigenvectors of graph (nodes,k)
        :param addBias: Add a bias to each node
        :param nonlinearity: Apply given nonlinearity (ie nn.Sigmoid)
        :param W: Initial weight matrix, size=(k,k)
        :param ongpu: Perform operations on the gpu
        """
        super(spectralLayer,self).__init__()
        if ongpu:
            self.U=Variable(U.float().cuda())
        else:
            self.U=Variable(U.float())

        if W is None:
            W=np.random.normal(0,0.01,(self.U.size()[1],self.U.size()[1]))

        self.W=nn.Parameter(torch.from_numpy(W).float())

        self.addBias=addBias
        if self.addBias:
            b=np.zeros((1,self.U.size()[0],1))
            self.b=nn.Parameter(torch.from_numpy(b).float())

        self.h=nonlinearity

        if ongpu:
            self.cuda()

    def forward(self,x):
        """
        Performs a forward pass of the spectral layer
        :param x: Feature Tensor, size=(batch,nodes,features)
        :return: New Feature Tensor, size=(batch,nodes,features)
        """
        y=torch.matmul(torch.t(self.U),x)
        y=torch.matmul(self.W,y)
        y=torch.matmul(self.U,y)

        if self.addBias:
            y=y+self.b
        if self.h is not None:
            y=self.h(y)

        return y

class GCN(nn.Module):
    """
    "Semi-Supervised Classification with Graph Convolutional Networks." Kipf, Thomas N. and Welling, Max
    """
    def __init__(self,fin,fout,adj,**kwargs):
        """
        :param fin: Input feature size
        :param fout: Ouput feature size
        :param adj: Normalized adjacency matrix \tilde{A}=(D+I)^(-1/2)(A+I)(D+I)^(-1/2)
        :param nonlinearity: Apply given nonlinearity (ie nn.Sigmoid)
        :param w: Initial weight matrix, size=(fin,fout)
        :param onGPU: Perform the operations on the GPU
        :param kwargs: nn.Module keywords
        """
        super(GCN, self).__init__()

        #A=adj+np.eye(adj.shape[0])
        #D=np.sqrt(np.maximum(L.sum(1),1))
        #np.diag(D)

        self.fin=fin
        self.fout=fout
        self.nodes=adj.shape[0]

        A=torch.DoubleTensor(adj)+torch.eye(adj.shape[0])
        D=torch.diag(torch.sqrt(1/torch.clamp(A.sum(1),min=1)))
        self.register_buffer("L",D.matmul(A).matmul(D))

        self.W=Linear(fin,fout)
        self.W=self.W.double()
        #init.uniform_(self.W.weight, -0.01, 0.01)
        #init.zeros_(self.W.bias)

    def forward(self,x):
        """
        Performs a forward pass of the graph convolution layer
        :param x: Feature Tensor, size=(batch,nodes,fin)
        :return: Diffused Feature Tensor, size=(batch,nodes,fout)
        """
        y=torch.matmul(self.L,x)
        y=self.W(y)
        return y

    def outShape(self,x=None):
        if x is None:
            return (-1,self.self.nodes,self.fout)
        elif type(x) is tuple:
            return (x[0],self.nodes,self.fout)
        return (x.shape[0],self.nodes,self.fout)

class GAT(nn.Module):
    """
    Simple GAT layer, similar to https://arxiv.org/abs/1710.10903
    """

    def __init__(self, in_features, out_features, adj, dropout=0, alpha=0.01, concat=True):
        super(GAT, self).__init__()
        self.dropout = dropout
        self.in_features = in_features
        self.out_features = out_features
        self.alpha = alpha
        self.concat = concat

        self.W = nn.Parameter(nn.init.xavier_uniform(torch.Tensor(in_features, out_features).type(torch.DoubleTensor), gain=np.sqrt(2.0)), requires_grad=True)
        self.a = nn.Parameter(nn.init.xavier_uniform(torch.Tensor(2*out_features, 1).type(torch.DoubleTensor), gain=np.sqrt(2.0)), requires_grad=True)
        self.register_buffer("adj",torch.from_numpy(adj))

        self.leakyrelu = nn.LeakyReLU(self.alpha)


    def forward(self, X):
        h = torch.matmul(X, self.W)
        B=h.size()[0]
        N = h.size()[1]
        a_input = torch.cat([h.repeat(1, 1, N).view(B, N * N, -1), h.repeat(1, N, 1)], dim=1).view(B,N, -1, 2 * self.out_features)
        e = self.leakyrelu(torch.matmul(a_input, self.a).squeeze(3))

        zero_vec = -9e15*torch.ones_like(e)
        attention = torch.where(self.adj > 0, e, zero_vec)
        attention = F.softmax(attention, dim=2)
        attention = F.dropout(attention, self.dropout, training=self.training)
        h_prime = torch.matmul(attention, h)

        if self.concat:
            return F.elu(h_prime)
        else:
            return h_prime

    def __repr__(self):
        return self.__class__.__name__ + ' (' + str(self.in_features) + ' -> ' + str(self.out_features) + ')'

    def outShape(self,x=None):
        if x is None:
            return (-1, self.out_features)
        if type(x) is tuple:
            return (x[-2], self.out_features)
        return (x.shape[-2], self.out_features)

class graphNet(nn.Module):
    def __init__(self, features, adj, glType, glSizes, embedding=None, dropout=0.0,
                 nodeLayers=None, nodeSummary=None, denseLayers=None, activations=None, finalActivation=None, **kwargs):
        super(graphNet, self).__init__()
        self.features=features
        self.adj=adj
        self.nodes=adj.shape[0]
        self.graphLayerName=glType
        self.graphLayers=nn.ModuleList()
        self.nodeLayers=nn.ModuleList()
        self.denseLayers=nn.ModuleList()
        self.activations=nn.ModuleList()
        self.dropout=nn.Dropout(p=dropout)

        #Embeddings convert one hot node feature vectors into dense vectors
        if embedding is not None:
            self.embedding=nn.Linear(features,embedding)
            currentShape=(self.nodes,embedding)
        else:
            self.embedding=None
            currentShape = (adj.shape[0], features)

        #Construct the graph layers
        if isinstance(glSizes,int):
            glSizes=[glSizes]
        for s in glSizes:
            name = str.lower(glType)
            layers = {"spatialqw": spatialQWNN,
                      "gcn": GCN,
                      "dcnn": DCNN,
                      "gat": GAT}
            if name in ["qw", "spatialqw", "temporalqw", "unitaryqw"]:
                self.graphLayers.append(layers[name](self.adj, s))
            elif name in ["gcn", "gat","dcnn"]:
                self.graphLayers.append(layers[name](currentShape[-1], s, self.adj))
            currentShape = self.graphLayers[-1].outShape(currentShape)

        #Node Layers act on each node's features independently of other nodes
        if nodeLayers is not None:
            for nL in nodeLayers:
                self.nodeLayers.append(Linear(currentShape[-1],nL))
                currentShape=(self.nodes,nL)

        #Node Summary is a function over nodes that reduces a node x features matrix to a summary feature vector
        nodeSummary=str.lower(nodeSummary)
        summaries={"cat":concatNodes,
                   "lstm":lstmEncoder,
                   "s2v":set2vec}
        if nodeSummary in ["cat","ave","max"]:
            self.nodeSummary = summaries[nodeSummary]()
            currentShape = self.nodeSummary.outShape(currentShape)
        elif nodeSummary in ["lstm"]:
            self.nodeSummary = summaries[nodeSummary](currentShape[1],currentShape[1])
            currentShape = self.nodeSummary.outShape(currentShape)
        elif nodeSummary in ["s2v"]:
            self.nodeSummary = summaries[nodeSummary](currentShape[1],12)
            currentShape = self.nodeSummary.outShape(currentShape)
        else:
            self.nodeSummary=None

        #Dense Layers act on the summarized graph or combine features across nodes
        if denseLayers is not None:
            for dL in denseLayers:
                self.denseLayers.append(Linear(currentShape[-1],dL))
                currentShape=(dL,)

        layerCount= len(self.graphLayers) + len(self.nodeLayers) + len(self.denseLayers)
        if activations is None:
            self.activations.extend([linearActivation()] * (layerCount - 1))
        elif isinstance(type(activations), nn.Module):
            self.activations.extend([activations()] * (layerCount - 1))
        else:
            self.activations.extend([a() for a in activations])

        if finalActivation is None:
            self.activations.append(linearActivation())
        else:
            self.activations.append(finalActivation())

    def forward(self,x,**kwargs):
        #init_amps=init_amps.float()
        #swap=swap.long()
        act=0
        if self.embedding is not None:
            x=self.embedding(x)
        for wL in self.graphLayers:
            x=wL(x)
            x=self.activations[act](x)
            act+=1
        x=self.dropout(x)
        for nL in self.nodeLayers:
            x=nL(x)
            x=self.activations[act](x)
            act+=1
        if self.nodeSummary:
            x = self.nodeSummary(x)
        for dL in self.denseLayers:
            x=dL(x)
            x=self.activations[act](x)
            act+=1
        return x
