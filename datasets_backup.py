import os
import networkx as nx
import numpy as np
import scipy as sp
import scipy.io
import torch
from sklearn.cluster import KMeans
from torch.utils.data import Dataset

from constructions import adj2list, adj2amps, adj2swap
from utils import label2vec

"""
for i in range(len(self.adj)):
    g=nx.from_numpy_matrix(self.adj[i])
    ord=np.argsort(nx.betweenness_centrality(g).values())[::-1]
    adj[i]=adj[i][ord,:][:,ord]
    #change other relevants
"""

class nme(Dataset):
    def __init__(self,name='mutag',reduce=False,
                 shuffleExamples=True,nodeOrder=nx.betweenness_centrality,
                 swapFunc=adj2swap,splits=[0.8,0.1,0.1],dataType=torch.FloatTensor):
        super(nme, self).__init__()
        if name=='mutag':
            keys=['lmutag','MUTAG']
        elif name=='enzymes':
            keys=['lenzymes','ENZYMES']
        elif name=='nci1':
            keys=['lnci1','NCI1']
        self.dataType=dataType
        data=sp.io.loadmat(os.path.join('data',keys[1]+'.mat'))
        self.Y=data[keys[0]].flatten()
        if name=='mutag':
            self.Y[self.Y==-1]=0
        elif name=='enzymes':
            self.Y=self.Y-1
        self.adj,self.X,self.nodes,self.degrees=[],[],[],[]
        for mut in data[keys[1]][0]:
            if name=='enzymes':
                adj=mut[0].toarray()
            elif name=='nci1':
                adj=mut[1].toarray()
            else:
                adj=mut[0]
            self.nodes.append(len(adj))
            self.degrees.append(np.int(np.max(np.sum(adj,axis=1)))+1)
            self.adj.append(adj)
            if name=='nci1':
                self.X.append(np.array(mut[0][0,0][0]).flatten())
            else:
                self.X.append(np.array(mut[1][0,0][0]).flatten())

        self.splits=np.array(splits)
        self.setidx=np.zeros(3,dtype=int)
        self.setidx[1]=np.ceil(len(self.X)*self.splits[0])
        self.setidx[2]=self.setidx[1]+np.ceil(len(self.X)*self.splits[1])

        #Order nodes according to given function
        for i in range(len(self.adj)):
            adj=self.adj[i]
            g = nx.from_numpy_matrix(adj)
            ord = np.argsort(nodeOrder(g).values())[::-1]
            self.adj[i] = adj[ord,:][:, ord]
            x=self.X[i]
            x=x[ord]
            self.X[i]=x

        if not reduce:
            maxdegree=np.max(self.degrees)
            if maxdegree%2==1:
                maxdegree+=1
            maxnodes=np.int(np.max(self.nodes))
            base=np.zeros((maxnodes,maxnodes))
            for i in range(len(self.adj)):
                newadj=np.array(base)
                newadj[:self.nodes[i],:self.nodes[i]]=self.adj[i]
                self.adj[i]=newadj

                newX=np.zeros(maxnodes)
                newX[:self.nodes[i]]=self.X[i]
                self.X[i]=newX
            self.nodes=[maxnodes]*len(self.adj)
            self.degrees=[maxdegree]*len(self.degrees)

        lset=np.unique(self.X)
        for i in range(len(self.X)):
            self.X[i]=label2vec(self.X[i],lset)[:,1:] #remove 0 label

        #Create Amplitude Matrices
        self.amps=[]
        self.swap=[]
        for i in range(len(self.adj)):
            self.amps.append(adj2amps(self.adj[i],self.degrees[i]))
            self.swap.append(swapFunc(self.adj[i],self.degrees[i]))

        if shuffleExamples:
            self.shuffle()

    def __len__(self):
        return self.setidx[1]

    def __getitem__(self, idx):
        if idx < 0  or idx >= self.setidx[1]:
            print("ERROR at "+str(idx))
        return (self.dataType(self.X[idx]),
                self.dataType(self.amps[idx]),
                torch.LongTensor(self.swap[idx]),
                self.Y[idx],
                self.dataType(self.adj[idx]))

    def valSet(self):
        return (self.dataType(self.X[self.setidx[1]:self.setidx[2]]),
                self.dataType(self.amps[self.setidx[1]:self.setidx[2]]),
                torch.LongTensor(self.swap[self.setidx[1]:self.setidx[2]]),
                self.Y[self.setidx[1]:self.setidx[2]],
                self.dataType(self.adj[self.setidx[1]:self.setidx[2]]))
    def testSet(self):
        return (self.dataType(self.X[self.setidx[2]:]),
                self.dataType(self.amps[self.setidx[2]:]),
                torch.LongTensor(self.swap[self.setidx[2]:]),
                self.Y[self.setidx[2]:],
                self.dataType(self.adj[self.setidx[2]:]))
    def shuffle(self):
        perm = np.random.permutation(len(self.Y))
        self.X = list(np.array(self.X)[perm])
        self.Y = np.array(self.Y)[perm]
        self.adj = list(np.array(self.adj)[perm])
        self.nodes = np.array(self.nodes)[perm]
        self.degrees = np.array(self.degrees)[perm]
        self.swap=list(np.array(self.swap)[perm])
        self.amps=list(np.array(self.amps)[perm])

class QM7(Dataset):
    def __init__(self,swapFunc=adj2swap,reduce=False,nodeOrder=nx.betweenness_centrality,fold=4,dataType=torch.FloatTensor):
        super(QM7, self).__init__()
        self.dataType=dataType

        data=sp.io.loadmat(os.path.join('data','qm7.mat'))
        self.atomicCharges=[]
        self.cartCoords=[]
        self.coulomb=[]
        self.amps=[]
        coulomb=data['X'] #7165,23,23
        if reduce:
            ac=data['Z']
            cC=data['R']
            coulomb = data['X']
            for i in range(len(data['Z'])):
                size=np.sum(ac[i]>0)
                self.atomicCharges.append(np.sort(ac[i,:size])[::-1]) #7165,var
                self.cartCoords.append(cC[i,:size,:]) #7165,var,3
                self.coulomb.append(np.array(coulomb[i,:size,:size])) #7165,var,var
        else:
            self.atomicCharges = list(data['Z'])  # 7165,23
            self.cartCoords=list(data['R']) #7165,23,3
            self.coulomb=list(np.array(data['X'])) #7165,23,23
        self.yLabels=data['T'].T #1,7165
        self.sizes=[np.sum(self.atomicCharges[i]>0) for i in range(len(self.atomicCharges))]

        #Folds
        self.splits=data['P']  # 5,1433
        self.trainsplit=[]
        self.testsplit=[]
        self.setFold(fold)

        #Convert Atomic Charges to 1 hot labels
        charges=[1,6,7,8,16] #Possible charge values
        #FOR SOME STUPID REASON THE DATASET DOESN'T GIVE CHARGES IN CORRECT ORDER
        print("Fixing Charges")
        self.atomicCharges=[]
        ccharges=[0,36,53,73,388]
        self.xLabels=[]
        for i in range(len(self.coulomb)):
            c=self.coulomb[i].diagonal()
            size=self.sizes[i]
            oneHot=np.zeros((len(c),len(charges)))
            atoms=np.zeros(len(self.coulomb[i]))
            for j in range(size):
                idx=np.argwhere(c[j]>ccharges).flatten()[-1]
                oneHot[j,idx]=1
                atoms[j]=charges[idx]
            self.atomicCharges.append(atoms)
            self.xLabels.append(oneHot)

        #Convert Coulomb Matrix to Similarity Matrices
        "Generating Similarity Matrices"
        self.sim=[]
        for molecule in range(len(self.coulomb)):
            cmat = np.array(self.coulomb[molecule])
            size = self.sizes[molecule]
            for i in range(size):
                charge = self.atomicCharges[molecule][i]
                if charge == 0:
                    break
                cmat[i, i] = 0
                for j in range(size):
                    cmat[i, j] = cmat[i, j] / charge
                    cmat[j, i] = cmat[j, i] / charge
            self.sim.append(np.array(cmat))

        #Convert Similarity Matrix to Graph
        if os.path.isfile(os.path.join('data','QM7Graphs.npy')):
            print("Loading Graphs from File")
            adj,thresh=np.load(os.path.join('data','QM7Graphs.npy'),encoding='latin1')
            self.adj=list(adj)
            self.thresholds=thresh
        else:
            print("Generating Graphs")
            adj = []
            thresholds = []
            for molecule in range(len(self.sim)):
                smat=np.array(self.sim[molecule])
                threshold=self.findThreshold(smat)
                thresholds.append(threshold)
                smat[smat>=threshold]=1
                smat[smat<threshold]=0
                adj.append(smat)
            self.thresholds=thresholds
            self.adj=adj
            print("Verifying Graphs")
            errs=self.verifyGraphs()
            print("Fixing "+str(len(errs))+" Graph Errors")
            self.fixGraphErrors(errs)

            #We want to make sure we are saving the graphs in max size
            if reduce:
                adj=[]
                maxsize=np.max(self.sizes)
                for i in range(len(self.adj)):
                    adj.append(np.zeros((maxsize,maxsize)))
                    adj[-1][:self.sizes[i],:self.sizes[i]]=self.adj[i]
                np.save(os.path.join('data','QM7Graphs.npy'),
                        (adj,self.thresholds))
            else:
                np.save(os.path.join('data', 'QM7Graphs.npy'),
                        (self.adj, self.thresholds))
        #Reduce Sizes
        if reduce:
            print("Reducing Graph Sizes")
            for i in range(len(self.adj)):
                size=self.sizes[i]
                self.adj[i]=self.adj[i][:size,:size]

        # Order nodes according to given function
        for i in range(len(self.adj)):
            adj = np.array(self.adj[i])
            g = nx.from_numpy_matrix(adj)
            perm = np.argsort(nodeOrder(g).values())[::-1]
            self.adj[i] = adj[perm, :][:, perm]
            self.sim[i] = self.sim[i][perm, :][:, perm]
            self.coulomb[i] = self.coulomb[i][perm, :][:, perm]
            self.atomicCharges[i] = self.atomicCharges[i][perm]
            self.cartCoords[i] = self.cartCoords[i][perm]


        #Calculate Bond Densities
        print("Calculating Bond Densities")
        self.bondDensity=[]
        for i in range(len(self.sim)):
            self.bondDensity.append(np.sum(self.sim[i])/(self.sizes[i]**2))

        #Calculate Nodes and Degrees in each molecule
        if reduce:
            self.nodes=[len(a) for a in self.adj]
            self.degrees=[np.int(np.max(np.sum(a,1))) for a in self.adj]
        else:
            self.nodes=[23]*len(self.adj)
            degree = 0
            for a in self.adj:
                d = np.int(np.max(np.sum(a, 1)))
                if d > degree:
                    degree = d
                if degree%2==1:
                    degree+=1
            self.degrees=[degree]*len(self.adj)

        #Create Amplitude Matrices
        print("Creating Iniitial Amplitude Matrices")
        self.amps=[]
        for i in range(len(self.adj)):
            amps=np.zeros((len(self.adj[i]),self.degrees[i],len(self.adj[i])))
            for j in range(len(amps)): #Put initial amps only on atom locations
                jdegree=np.int(np.sum(self.adj[i][j]))
                if jdegree==0:
                    continue
                amps[j, :jdegree, j] = 1. / np.sqrt(jdegree)
            self.amps.append(np.array(amps))

        # Convert Adjacency Matrices to Swap Tensors
        print("Creating Swap Arrays")
        self.swap=[]
        for i in range(len(self.adj)):
            self.swap.append(swapFunc(self.adj[i], self.degrees[i]))
            # swap.append(np.zeros((2,self.nodes[i]*self.degrees[i]),dtype=int))
            # a,b=[],[]
            # inds=np.zeros(self.nodes[i])
            # for j in range(len(self.adj[i])):
            #     neighbors=np.argwhere(self.adj[i][j]==1).flatten()
            #     for n in range(self.degrees[i]):
            #         if n < len(neighbors):
            #             a.append(neighbors[n])
            #             b.append(inds[neighbors[n]])
            #             inds[neighbors[n]]+=1
            #         else:
            #             a.append(j)
            #             b.append(n)
            # swap[-1][0]=a
            # swap[-1][1]=b
            # self.swap=swap

        #Create Permutation Tensor
        print("Creating Permutation Tensor")
        permute=[]
        for i in range(len(self.adj)):
            adj=self.adj[i]
            n = adj.shape[0]
            d = self.degrees[i]
            swap = np.zeros((n, d, n, d))
            ind = np.zeros(n, dtype=int)

            for i in range(n):
                for j in np.arange(i + 1, n):
                    if adj[i, j] == 1:
                        swap[i, ind[i], j, ind[j]] = 1
                        swap[j, ind[j], i, ind[i]] = 1
                        ind[i] += 1
                        ind[j] += 1
            permute.append(swap)
        self.permute=permute


    def findThreshold(self,x):
        vals = np.array(x.flatten())
        vals = vals[vals > 0]
        labels = KMeans(2).fit(vals.reshape(-1, 1)).labels_
        t = np.maximum(np.min(vals[labels == 1]), np.min(vals[labels == 0]))
        return t

    #Hacky, not-optimal, makes sure all graphs are connected
    def verifyGraphs(self,graphs=None):
        errs=[]
        if graphs is None:
            graphs=self.adj
        for i in range(len(graphs)):
            a=np.array(graphs[i])
            s=[np.zeros(len(a))]
            s[-1][0]=len(a)
            for j in range(20):
                s.append(a.dot(s[-1]))
                if np.sum(s[-1]==0)==0:
                    break
            if np.sum((s[-1]+s[-2])==0)>0:
                errs.append(i)
        return errs

    #Any disconnected graph has its threshold iteratively lowered until connected
    def fixGraphErrors(self,errs=None):
        if errs is None:
            errs=self.verifyGraphs()
        for e in errs:
            vals=np.array(self.sim[e]).flatten()
            vals=vals[vals>0]
            vals=np.sort(vals)[::2]
            adj=np.array(self.adj[e])
            thresh=self.thresholds[e]
            while len(self.verifyGraphs([adj]))>0:
                idx=len(vals)-1
                while vals[idx]>=thresh:
                    idx-=1
                    if idx<0:
                        print("Error! idx<0")
                        return
                thresh=vals[idx]
                adj=np.array(self.sim[e])
                adj[adj>=thresh]=1
                adj[adj<thresh]=0
            self.adj[e]=np.array(adj)
            self.thresholds[e]=thresh

    def verifyCharges(self):
        errs=[]
        for i in range(len(self.coulomb)):
            for j in range(self.sizes[i]):
                if np.abs(0.5*np.power(self.atomicCharges[i][j],2.4)-self.coulomb[i][j,j])>1:
                    errs.append(i)
                    break
        return errs

    def __len__(self):
        return len(self.trainsplit)

    def __getitem__(self,idx):
        idx=self.trainsplit[idx]
        return (self.dataType(self.xLabels[idx]),
               self.dataType(self.amps[idx]),
               torch.LongTensor(self.swap[idx]),
               self.dataType(self.yLabels[idx]),
               self.dataType(self.adj[idx]))

    def testSet(self):
        ts=[]
        for idx in self.testsplit:
            ts.append((self.dataType(self.xLabels[idx]),
               self.dataType(self.amps[idx]),
               torch.LongTensor(self.swap[idx]),
               self.dataType(self.yLabels[idx]),
               self.dataType(self.adj[idx])))
        return ts

    def trainSet(self):
        x,a,s,y=[],[],[],[]
        ts=[]
        for idx in self.trainsplit:
            ts.append((self.dataType(self.xLabels[idx]),
                       self.dataType(self.amps[idx]),
                       torch.LongTensor(self.swap[idx]),
                       self.dataType(self.yLabels[idx]),
                       self.dataType(self.adj[idx])))
        return ts

    def setFold(self,fold):
        self.trainsplit = [a for i in range(len(self.splits)) if i != fold for a in self.splits[i]]
        self.testsplit = self.splits[fold]

class numpyDataset(Dataset):
    def __init__(self,datafile='data/ustemp/2009.npy',adjfile='data/ustemp/2009adj.npy',
                 offset=1,trainRatio=1,shuffleEx=False,shuffleNodes=False,
                 dropout_p=0.0,dataType=torch.FloatTensor):

        data = np.load(datafile).astype(float)
        if len(data.shape)==2:
            data=np.expand_dims(data,-1)

        self.dataX=data[:-offset]
        self.dataY=data[offset:]
        self.adj = np.load(adjfile).astype(int)
        self.dropout_p=dropout_p

        if shuffleEx:
            perm = np.random.permutation(len(self.dataX))
            self.dataX=self.dataX[perm]
            self.dataY=self.dataY[perm]

        if shuffleNodes:
            perm=np.random.permutation(len(self.adj))
            self.dataX=self.dataX[:,perm]
            self.dataY=self.dataY[:,perm]
            self.adj=self.adj[perm][:,perm]

        self.dataType=dataType
        self.dataX=self.dataType(self.dataX)
        self.dataY=self.dataType(self.dataY)
        self.adj_list=adj2list(self.adj,torch.from_numpy)
        self.adj=self.dataType(self.adj)

        self.offset=offset
        self.trainRatio=trainRatio

    def __len__(self):
        #Returns the length of the train set
        return np.int(len(self.dataX)*self.trainRatio)

    def __getitem__(self,idx):
        #Returns an item from the train set
        mask = np.random.choice([0, 1], self.dataX[idx].shape, p=[self.dropout_p, 1 - self.dropout_p])
        return (self.dataX[idx] * self.dataType(mask), self.dataY[idx])

    def testSet(self):
        #Returns the entire test setexperiment,
        return (self.dataX[self.__len__():],
                self.dataY[self.__len__():])

    def trainSet(self):
        #Returns the entire train set
        return (self.dataX[:self.__len__()],
                self.dataY[:self.__len__()])

class coraDataset(Dataset):
    def __init__(self,path="data/cora/",dataset="cora",
                 length=None,shuffleNodes=False,dropout_p=0.0):
        label2index = {
            'Case_Based': 0,
            'Genetic_Algorithms': 1,
            'Neural_Networks': 2,
            'Probabilistic_Methods': 3,
            'Reinforcement_Learning': 4,
            'Rule_Learning': 5,
            'Theory': 6
        }

        idx_features_labels = np.genfromtxt("{}{}.content".format(path, dataset), dtype=np.dtype(str))
        self.dataX = np.array(idx_features_labels[:, 1:-2], dtype=float)
        self.dataY = np.array([label2index[i] for i in idx_features_labels[:, -1]])

        idx = np.array(idx_features_labels[:, 0], dtype=np.int32)
        idx_map = {j: i for i, j in enumerate(idx)}
        edges_unordered = np.genfromtxt("{}{}.cites".format(path, dataset), dtype=np.int32)
        edges = np.array(list(map(idx_map.get, edges_unordered.flatten())),
                         dtype=np.int32).reshape(edges_unordered.shape)

        adj = np.zeros((self.dataY.shape[0], self.dataY.shape[0]))
        for e in edges:
            adj[e[0],e[1]]=1
        adj = adj + adj.T
        adj[adj>0]=1
        self.adj=adj

        if shuffleNodes:
            perm=np.random.permute(len(adj))
            self.adj=self.adj[perm][:,perm]
            self.dataX=self.dataX[perm]
            self.dataY=self.dataY[perm]

        self.adj_list=adj2list(self.adj,torch.from_numpy)

        if length is None:
            self.length=len(self.dataX)
        else:
            self.length=length

        self.dropout_p=dropout_p

    def __len__(self):
        return len(self.dataX)

    def __getitem__(self, idx):
        mask=np.random.choice([0,1], self.dataX.shape, p=[self.dropout_p, 1 - self.dropout_p])
        out=(torch.from_numpy(self.dataX * mask),
             torch.from_numpy(self.dataY))
        return out

    def trainSet(self):
        out = (torch.from_numpy(self.dataX), torch.from_numpy(self.dataY))

    def testSet(self):
        out=(torch.from_numpy(self.dataX), torch.from_numpy(self.dataY))
